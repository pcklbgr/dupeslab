### User Option File

Your average user option file:

```ini
[mysql]
user=user.name
password='secret;password'
host=localhost

[mysqladmin]
user=user.name
password='secret;password'
host=localhost
```

Note that password needs to be quoted, especially if it has symbols like `;` or `=`.

---
- [Using Option Files](https://dev.mysql.com/doc/refman/5.7/en/option-files.html) (look for _typical user option file_).
- [mysql .my.cnf not reading credentials properly?](https://stackoverflow.com/a/19505656)
