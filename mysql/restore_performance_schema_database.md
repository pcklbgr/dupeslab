### Restore `performance_schema` Database

To recover lost (e.g. deleted) `performance_schema` database run:

```
mysql_upgrade -u root -p --force
```

And restart MySQL service.

---
- [Table 'performance_schema.session_variables' doesn't exist](https://stackoverflow.com/a/48029266)
