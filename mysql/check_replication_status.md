### Check Replication Status

Slave replication status:

```sql
SHOW SLAVE STATUS\G
```

`Slave_IO_Running` and `Slave_SQL_Running` should say _yes_; `Exec_Master_Log_Pos` should be going up.

Slave list (on master):

```sql
SHOW SLAVE HOSTS;
```

---
- [Checking Replication Status](https://dev.mysql.com/doc/refman/5.7/en/replication-administration-status.html)
- [SHOW SLAVE STATUS Syntax](https://dev.mysql.com/doc/refman/5.7/en/show-slave-status.html)
