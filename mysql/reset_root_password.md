### Reset _root_ Password

Stop MySQL any running instance and run mysqld process with `--skip-grant-tables`:

```
mysqld_safe --skip-grant-tables &
```

Connect to MySQL as _root_:

```
mysql -u root
```

Set new password (in MySQL _5.7.5_ and earlier)

```
use mysql;
update user set password=PASSWORD("SecretPassword") where User='root';
flush privileges;
quit
```

---
- [Setting, Changing And Resetting MySQL Root Passwords](https://www.howtoforge.com/setting-changing-resetting-mysql-root-passwords)
