### Process List

To show MySQL process list:

```
SHOW FULL PROCESSLIST\G
```

Which can be replaced with:

```
SELECT * FROM information_schema.processlist
```

e.g. to select only `SELECT` statements from process list:

```
SELECT * FROM information_schema.processlist WHERE `INFO` LIKE 'SELECT %';
```

---
- [SHOW PROCESSLIST Syntax](https://dev.mysql.com/doc/refman/5.7/en/show-processlist.html)
- [how to customize `show processlist` in mysql?](https://stackoverflow.com/a/23549163)
- [MySQL: How To Add Where Clause To Processlist](http://www.stetsenko.net/2011/08/mysql-how-to-add-where-clause-to-processlist/)
