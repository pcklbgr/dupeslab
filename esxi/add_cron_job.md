### Add _cron_ Job

Kill existing `crond` process:

```
/bin/kill $(cat /var/run/crond.pid)
```

Edit `/var/spool/cron/crontabs/root`, standard _cron_ syntax works.
File might be read-only, if so temporarilly add write permissions with `chmod +w /var/spool/cron/crontabs/root`.

Start `crond` service:
```
/usr/lib/vmware/busybox/bin/busybox crond
```

---
- [Add cron Job to VMware ESX/ESXi](http://www.jules.fm/Logbook/files/add_cron_job_vmware.html)
- [GhettoVBC, ESXi 5.5 & Crontab](https://www.stephen-scotter.net/computers/virtualisation/ghettovbb-esxi-5-5-crontab)
- [KB1033346](https://kb.vmware.com/s/article/1033346) - Gathering esxtop performance data at specific times using crontab
