### Install _.vib_ Package

To install _.vib_ package from zip file:

```
esxcli software vib install -d "/vmfs/volumes/datastore1/patch-directory/ESXi500-201111001.zip"
```

Command does not like relative paths.

---
- [KB2008939](https://kb.vmware.com/s/article/2008939) - _“esxcli software vib” commands to patch an ESXi 5.x/6.x host_
