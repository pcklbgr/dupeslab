### Remove _SSH for the host has been enabled_ Warning

To remove warning _SSH for the host has been enabled_ or _ESXi Shell for the Host has been enabled_:

- Go to ESXi's Host _Configuration_;
- Click _Advanced Settings_ under _Software_;
- Set value for _UserVars.SuppressShellWarning_ to *1* under _UserVars_.

---
- [Cluster warning for ESXi Shell and SSH appear on an ESXi 5.x host (2003637)](https://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=2003637)
- [How to – get rid of the “SSH for the host has been enabled” warning…](http://www.running-system.com/how-to-get-rid-of-the-ssh-for-the-host-has-been-enabled-warning/)
