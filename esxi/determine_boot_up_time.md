### Determine Boot Up Time

Check `/var/log/vmksummary.log`:

```
grep booted /var/log/vmksummary.log
```

---
- [When did my ESXi host boot?](http://vmwarebits.com/content/when-did-my-esxi-host-boot)
- [Determining why an ESXi/ESX host was powered off or restarted (1019238)](https://kb.vmware.com/s/article/1019238)
