### Change Repository URL

For SVN versions older than _1.7_:

```
svn switch --relocate OLD_URL NEW_URL
```

For later versions:

```
svn relocate NEW_URL
```

---
- [`svn switch`](http://svnbook.red-bean.com/en/1.6/svn.ref.svn.c.switch.html)
- [`svn relocate`](http://svnbook.red-bean.com/en/1.7/svn.ref.svn.c.relocate.html)
- [Change SVN repository URL](https://stackoverflow.com/a/13944343)
