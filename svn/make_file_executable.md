### Make File Executable

To add _executable bit_:

```
svn propset svn:executable on FILE
```

To remove _executable bit_:

```
svn propdel svn:executable FILE
```

---
- [`svn propset`](http://svnbook.red-bean.com/en/1.7/svn.ref.svn.c.propset.html)
- [Proper way to add svn:executable](https://stackoverflow.com/a/5757365)
