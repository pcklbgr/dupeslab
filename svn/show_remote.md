### Show Remote

Prints information about current working copy:

```
svn info .
```

---
- `svn info` [manpage](http://svnbook.red-bean.com/en/1.6/svn.ref.svn.c.info.html)
- [How to get svn remote repository URL?](http://stackoverflow.com/a/9128385)
