### Force Commit Without Changes

Change one of the properties of file and commit afterwards:

```
svn propset foo 1 file.txt
svn commit -m "commit without changes"
```

---
- [`svn propset`](http://svnbook.red-bean.com/en/1.7/svn.ref.svn.c.propset.html)
- [How can I force subversion to commit an unchanged file?](https://stackoverflow.com/a/206856)
