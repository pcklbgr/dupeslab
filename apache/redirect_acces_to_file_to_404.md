### Redirect Access to File to 404

Use `mod_alias` _RedirectMatch_. e.g. to access to `.git*` files to 404:

```
RedirectMatch 404 \.git.*
```

---
- `mod_alias` [_RedirectMatch_](https://httpd.apache.org/docs/2.4/mod/mod_alias.html#redirectmatch)
- [How to deny the web access to some files?](https://serverfault.com/a/22591)
