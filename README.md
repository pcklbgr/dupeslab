Dupe Slab
---------

Various bits of useful information (cheat sheet).

### Categories

- [Ansible](#ansible)
- [Apache](#apache)
- [Avamar](#avamar)
- [Cygwin](#cygwin)
- [Elasticsearch](#elasticsearch)
- [VMware ESXi](#vmware-esxi)
- [FreeBSD](#freebsd)
- [Git](#git)
- [Gradle](#gradle)
- [Groovy](#groovy)
- [ImageMagick](#imagemagick)
- [Java](#java)
- [Jenkins](#jenkins)
- [Linux](#linux)
- [Misc](#misc)
- [MongoDB](#mongodb)
- [MySQL](#mysql)
- [Sonatype Nexus](#sonatype-nexus)
- [NGiИX](#ngiиx)
- [Perl](#perl)
- [PHP](#php)
- [Jenkins Pipeline Plugin](#jenkins-pipeline-plugin)
- [Postgres](#postgres)
- [PowerShell](#powershell)
- [Puppet](#puppet)
- [Python](#python)
- [Razor](#razor)
- [Redis](#redis)
- [ReGex](#regex)
- [MikroTik RouterOS](#mikrotik-routeros)
- [Ruby](#ruby)
- [Shell](#shell)
- [Sublime Text 3](#sublime-text-3)
- [Subversion](#subversion)
- [Vagrant](#vagrant)
- [Vim](#vim)
- [VirtualBox](#virtualbox)
- [Win](#win)
- [Zabbix](#zabbix)

---

#### Ansible

- [Conditional Role](ansible/conditional_role.md)
- [Config](ansible/config.md)
  - [Override Config Value On the Fly](ansible/config.md#override-config-value-on-the-fly)
  - [Formatted Error Output](ansible/config.md#formatted-error-output)
  - [Disable _.retry_ Files](ansible/config.md#disable-retry-files)
- [Escape Quotes in Variable](ansible/escape_quotes_in_variable.md)
- [Exclude Host With `--limit`](ansible/exclude_host_with_limit.md)
- [Force Handlers to Run](ansible/force_handlers_to_run.md)
- [Include Tasks from a File](ansible/include_tasks_from_a_file.md)
- [List All `ansible_` Variables](ansible/list_all_ansible__variables.md)
- [Reboot Machine and Wait For It to Come Back Up](ansible/reboot_machine_and_wait_for_it_to_come_back_up.md)
- [Remote Fileglob](ansible/remote_fileglob.md)
- [Run Command on Remote Hosts](ansible/run_command_on_remote_hosts.md)
- [_SSL: CERTIFICATE_VERIFY_FAILED_ Error](ansible/ssl_certificate_verify_failed_error.md)
- [Supress Task Warnings](ansible/supress_task_warnings.md)
- [Unsafe Strings](ansible/unsafe_strings.md)

#### Apache

- [Redirect Access to File to 404](apache/redirect_acces_to_file_to_404.md)

#### Avamar

- [REST API on AVE _7.3-233_](avamar/rest_api_on_ave_7.3-233.md)
  - [Fix `curl: (35) SSL connect error`](avamar/rest_api_on_ave_7.3-233.md#fix-curl-35-ssl-connect-error)

#### Cygwin

- [Convert Between UNIX and Windows Paths](cygwin/convert_between_unix_and_windows_paths.md)

#### Elasticsearch

- [Shards](elasticsearch/shards.md)
  - [Show Shard Allocation](elasticsearch/shards.md#show-shard-allocation)
  - [List _UNASSIGNED_ Shards](elasticsearch/shards.md#list-unassigned-shards)
  - [Retry Shard Allocation](elasticsearch/shards.md#retry-shard-allocation)
- [Turn Off Read-Only Mode](elasticsearch/turn_off_read-only_mode.md)

#### VMware ESXi

- [Add _cron_ Job](esxi/add_cron_job.md)
- [Determine Boot Up Time](esxi/determine_boot_up_time.md)
- [Install _.vib_ Package](esxi/install_vib_package.md)
- [Remove _SSH for the host has been enabled_ Warning](esxi/remove_ssh_for_the_host_has_been_enabled_warning.md)

#### FreeBSD

- [CPU Info](freebsd/cpu_info.md)
- [List Kernel Modules](freebsd/list_kernel_modules.md)
- [List Open Ports](freebsd/list_open_ports.md)
- [Remount Partition with New Options](freebsd/remount_partition_with_new_options.md)
- [Show Routing Table](freebsd/show_routing_table.md)

#### Git

- [Add Remote](git/add_remote.md)
- [Add Remote to Fresh Repository](git/add_remote_to_fresh_repository.md)
- [Apply patch](git/apply_patch.md)
- [Change Commit Author](git/change_commit_author.md)
  - [Change Commit Author by Rewriting History](git/change_commit_author.md#change-commit-author-by-rewriting-history)
- [Change Remote URL](git/change_remote_url.md)
- [Check if `git` Treats File as Binary](git/check_if_git_treats_file_as_binary.md)
- [Clone Branch](git/clone_branch.md)
- [Colorized Output](git/colorized_output.md)
- [Current Commit/Branch](git/current_commit_branch.md)
- [Dealing with Line Endings on Windows](git/dealing_with_line_endings_on_windows.md)
- [Disable Advices](git/disable_advices.md)
- [Discard Changes in a File](git/discard_changes_in_a_file.md)
- [`.gitattributes` and `.gitignore`](git/gitattributes_and_gitignore.md)
  - [Negate Ignore Pattern](git/gitattributes_and_gitignore.md#negate-ignore-pattern)
  - [Treat Files as Binary](git/gitattributes_and_gitignore.md#treat-files-as-binary)
- [Empty Commit](git/empty_commit.md)
- [Handy Aliases](git/handy_aliases.md)
- [History of a Range of Lines](git/history_of_a_range_of_lines.md)
- [History of Moved File](git/history_of_moved_file.md)
- [List Ignored Files](git/list_ignored_files.md)
- [List Untracked Files](git/list_untracked_files.md)
- [Log Graph](git/log_graph.md)
- [Log Last Week](git/log_last_week.md)
- [Make File Executable](git/make_file_executable.md)
- [Multiline Commit Message](git/multiline_commit_message.md)
- [Push Local Branch to Remote](git/push_local_branch_to_remote.md)
- [Rename Branch](git/rename_branch.md)
- [Remove File from Checkin](git/remove_file_from_checkin.md)
- [Show First Commit](git/show_first_commit.md)
- [Show Config](git/show_config.md)
- [Submodules](git/submodules.md)
- [Unpushed Changes](git/unpushed_changes.md)
- [Unstage Staged File](git/unstage_staged_file.md)

#### Gradle

- [Copy Files](gradle/copy_files.md)
- [Delete File Tree](gradle/delete_file_tree.md)
- [Log to File](gradle/log_to_file.md)
- [Use Local Plugin](gradle/use_local_plugin.md)

#### Groovy

- [Ignore Self-signed Cert Errors](groovy/ignore_self-signed_cert_errors.md)

#### ImageMagick

- [Convert Image to _.pdf_](imagemagick/convert_image_to_pdf.md)
- [Batch Convert Format](imagemagick/batch_convert_format.md)

#### Java

- [Default Command Line Flags](java/default_command_line_flags.md)

#### Jenkins

- [Clear Job Build History](jenkins/clear_job_build_history.md)
- [Disable Security](jenkins/disable_security.md)
- [Download All Artifacts](jenkins/download_all_artifacts.md)
- [Get Build Console Output](jenkins/get_build_console_output.md)
- [List Plugins](jenkins/list_plugins.md)
- [Restart Jenkins Instance](jenkins/restart_jenkins_instance.md)
- [Troubleshooting](jenkins/troubleshooting.md)
  - [Error on Accessing Internal Git Repo: _no matching key exchange method found. Their offer: diffie-hellman-group1-sha1_](jenkins/troubleshooting.md#error-on-accessing-internal-git-repo-no-matching-key-exchange-method-found-their-offer-diffie-hellman-group1-sha1)

#### Linux

- [Add Host to `known_hosts`](linux/add_host_to_known_hosts.md)
- [Add Kernel Parameters to `grub.conf`](linux/add_kernel_parameters_to_grub.conf.md)
- [Bootable Debian USB Drive](linux/bootable_debian_usb_drive.md)
- [Check if Apache is Using Prefork or Worker](linux/check_if_apache_uses_prefork_or_worker.md)
- [Check if IPv6 is Enabled](linux/check_if_ipv6_is_enabled.md)
- [Check if Machine is Virtual](linux/check_if_machine_is_virtual.md)
- [Check _.tar.gz_ File](linux/check_tar_gz_file.md)
- [Concatenate Multiple Lines](linux/concatenate_multiple_lines.md)
- [Convert _.flac_ to _.mp3_](linux/convert_flac_to_mp3.md)
- [Convert _.ppk_ to _rsa_ key](linux/convert_ppk_to_rsa_key.md)
- [`cp`](linux/cp.md)
  - [Copy if File Exists](linux/cp.md#copy-if-file-exists)
- [`curl`](linux/curl.md)
  - [Display HTTP Status Code](linux/curl.md#display_http_status_code)
  - [Display Headers](linux/curl.md#display_headers)
- [`deb` Packages](linux/deb_packages.md)
  - [Find Package That Provides File](linux/deb_packages.md#find-package-that-provides-file)
  - [List Available Package Versions for `apt`](linux/deb_packages.md#list-available-package-versions-for-apt)
  - [Restore Configuration Files](linux/deb_packages.md#restore-configuration-files)
  - [List Packages Available for Upgrade](linux/deb_packages.md#list-packages-available-for-upgrade)
- [Detect IP Address Conflicts](linux/detect_ip_address_conflicts.md)
- [Determine gzip'ed File Size](linux/determine_gzipped_file_size.md)
- [DHCP Subnet to Interface Mapping](linux/dhcp_subnet_to_interface.md)
- [Difference Between `.bashrc` and `.bash_profile`](linux/difference_between_.basrc_and_.bash_profile.md)
- [Disable _Predictable Network Interface Names_](linux/disable_predictable_network_interface_names.md)
  - [CentOS 7 Install](linux/disable_predictable_network_interface_names.md#centos-7-install)
- [Disable Printing on Samba](linux/disable_printing_on_samba.md)
- [Disable Transparent Huge Pages](linux/disable_transparent_huge_pages.md)
- [Disconnect `telnet` Automagically](linux/disconnect_telnet_automagically.md)
- [Disk Usage of Hidden Files](linux/disk_usage_of_hidden_files.md)
- [Download Youtube Videos](linux/download_youtube_videos.md)
- [Edit Command from `history`](linux/edit_command_from_history.md)
- [Extract Text from _pdf_ File](linux/extract_text_from_pdf_file.md)
- [File Lines](linux/file_lines.md)
- [`find`](linux/find.md)
  - [Copy Files from Subdirectories to a Folder](linux/find.md#copy-files-from-subdirectories-to-a-folder)
  - [Find Files by Extension and Count Lines](linux/find.md#find-files-by-extension-and-count-lines)
  - [Find Files Older Than _n_ Days](linux/find.md#find-files-older-than-n-days)
- [Flush `nscd` DNS Cache](linux/flush_nscd_dns_cache.md)
- [Force Log Rotation](linux/force_log_rotation.md)
- [Generate _"Random"_ MAC Address](linux/generate_random_mac_address.md)
- [Generate `udev` `70-persistent-net.rules`](linux/generate_udev_70-persistent-net.rules.md)
- [`git` Information in Bash Shell Prompt](linux/git_information_in_bash_shell_prompt.md)
- [Grant Access to `crontab`](linux/grant_access_to_crontab.md)
- [`grep` Binary Files Like Text](linux/grep_binary_files_like_text.md)
- [`history` Time Stamps](linux/history_time_stamp.md)
- [Line Endings](linux/line_endings.md)
- [Mount DFS share](linux/mount_dfs_share.md)
- [_nohup_ Running Process](linux/nohup_running_process.md)
- [Install Debian Jessie Without _systemd_](linux/install_debian_jessie_without_systemd.md)
- [Non-home Samba Shares Under SELinux](linux/non-home_samba_shares_under_selinux.md)
- [OpenSSL](linux/openssl.md)
  - [View Certificate Details](linux/openssl.md#view-certificate-details)
  - [View PKCS#12 Certificate](linux/openssl.md#view-pkcs12-certificate)
  - [Check if Certificate Matches Private Key](linux/openssl.md#check-if-certificate-matches-private-key)
  - [View Certificate Signing Request Details](linux/openssl.md#view-certificate-signing-request-details)
  - [View Certificate Chain Details](linux/openssl.md#view-certificate-chain-details)
  - [Certificate File Formats](linux/openssl.md#certificate-file-formats)
  - [Convert PKCS#7 to PEM](linux/openssl.md#convert-pkcs7-to-pem)
  - [Export PEM to PKCS#12](linux/openssl.md#export-pem-to-pkcs12)
- [Parse Filename from URL](linux/parse_filename_from_url.md)
- [Ping with _MTU 9000_ packets](linux/ping_with_mtu_9000_packets.md)
- [Recursive `wget`](linux/recursive_wget.md)
- [Reload `.bashrc`](linux/reload_bashrc.md)
- [Remove Hidden Files](linux/remove_hidden_files.md)
- [Remove Spaces at the End of the Line](linux/remove_spaces_at_the_end_of_the_line.md)
- [Rename Network Interface on Debian](linux/rename_network_interface_on_debian.md)
- [Reread Partition Table](linux/reread_partition_table.md)
- [Reverse Word Order in a String](linux/reverse_word_order_in_a_string.md)
- [`rpm` Packages](linux/rpm_packages.md)
  - [Reinstall Package](linux/rpm_packages.md#reinstall-package)
  - [Find Package That Provides File](linux/rpm_packages.md#find-package-that-provides-file)
  - [Show `rpm` file information](linux/rpm_packages.md#show-rpm-file-information)
  - [List Files in `rpm` Package](linux/rpm_packages.md#list-files-in-rpm-package)
  - [List _requires_ for `rpm` file](linux/rpm_packages.md#list-requires-for-rpm-file)
  - [List _provides_ in `rpm` file](linux/rpm_packages.md#list-provides-in-rpm-file)
  - [Extract `rpm` File](linux/rpm_packages.md#extract-rpm-file)
  - [`rpm` Query](linux/rpm_packages.md#rpm-query)
  - [Installed _gpg_ Signatures Information](linux/rpm_packages.md#installed-gpg-signatures-information)
  - [List `yum` variables](linux/rpm_packages.md#list-yum-variables)
- [Run Command in New `tmux` Session and Detach](linux/run_command_in_new_tmux_session_and_detach.md)
- ~~[Samba Shares Under SELinux](linux/samba_shares_under_selinux.md)~~ <span style="color:red">can't get this to work</span>
- [Search Bash History](linux/search_bash_history.md)
- [Send String to `stdin`](linux/send_string_to_stdin.md)
- [_SSHA_ Encrypted Password](linux/ssha_encrypted_password.md)
- [Switch PulseAudio Output](linux/switch_pulseaudio_output.md)
- [Trigger Creation of `70-persistent-net.rules`](linux/trigger_creation_of_70-persistent-net.rules.md)
- [Unban IP in Fail2ban](linux/unban_ip_in_fail2ban.md)
- [View Public Key of Private SSH Key](linux/view_public_key_of_private_ssh_key.md)
- [View SELinux Context](linux/view_selinux_context.md)
- [View Windows Executable Version](linux/view_windows_executable_version.md)
- [_vsftpd_ Setup](linux/vsftpd_setup.md)
  - [Debian Wheezy](linux/vsftpd_setup.md#debian-wheezy)
- [Wildcard SSH _Host_](linux/wildcard_ssh_host.md)
- [Write Multi-line String to File](linux/write_multi-line_string_to_file.md)

#### Misc

- [Capture Page Screenshot in Chrome](misc/capture_page_screenshot_in_chrome.md)
- [FTP-SSL with Total Commander](misc/ftp-ssl_with_total_commander.md)
- [Get Rid of Unread Emails in Outlook](misc/get_rid_of_unread_emails_in_outlook.md)
- [Ignore Application with Ditto](misc/ignore_application_with_ditto.md)
- [`npm` Loglevel](misc/npm_loglevel.md)
- [Paste with `shift-insert` from Ditto](misc/paste_with_shift-insert_from_ditto.md)

#### MongoDB

- [Check Storage Engine in Use](mongodb/check_storage_engine_in_use.md)
- [Evaluate Command](mongodb/evaluate_command.md)
- [Query Secondary Replica Set Members](mongodb/query_secondary_replica_set_members.md)

#### MySQL

- [Check Replication Status](mysql/check_replication_status.md)
- [Process List](mysql/process_list.md)
- [Reset _root_ Password](mysql/reset_root_password.md)
- [Restore `performance_schema` Database](mysql/restore_performance_schema_database.md)
- [User Option File](mysql/user_option_file.md)

#### Sonatype [Nexus](https://www.sonatype.com/nexus-repository-oss)

- [Get List of Files in Repo](nexus/get_list_of_files_in_repo.md)
- [Upload Files](nexus/upload_files.md)

#### NGiИX

- [Catch-all HTTP to HTTPS Redirect](nginx/catch-all_http_to_https_redirect.md)

#### Perl

- [Sort Array Numerically](perl/sort_array_numerically.md)

#### PHP

- [Compile with `libmysqlclient`](php/compile_with_libmysqlclient.md)
- [`phpinfo`](php/phpinfo.md)

#### Jenkins [Pipeline](https://jenkins.io/doc/book/pipeline/) Plugin

- [Check if File Exists](pipeline/check_if_file_exists.md)
- [Get Command Output](pipeline/get_command_output.md)
- [Print File to Output](pipeline/print_file_to_output.md)
- [Set `$PATH`](pipeline/set_path.md)

#### Postgres

- [Check Replication Status](postgres/check_replication_status.md)
- [Disk Usage](postgres/disk_usage.md)
- [Run Queries from CLI](postgres/run_queries_from_cli.md)

#### PowerShell

- [Active Directory Module](powershell/active_directory_module.md)
- [Check if File Exists](powershell/check_if_file_exists.md)
- [Check if User Account is Locked/Password Expired](powershell/check_if_user_account_is_locked_password_expired.md)
- [Command-line Parameters](powershell/command-line_parameters.md)
- [Determine Installed Version](powershell/determine_installed_version.md)
- [Exit Code of `Start-Process`](powershell/exit_code_of_start-process.md)
- [Get Directory Name of the Script](powershell/get_directory_name_of_the_script.md)
- [Mount and Use Network Share](powershell/mount_and_use_network_share.md)
- [Network Information](powershell/network_information.md)
- [Run Script](powershell/run_script.md)
- [Wait for _Any Key_](powershell/wait_for_any_key.md)

#### Puppet

- [Check Syntax](puppet/check_syntax.md)
- [Iteration with Index in Templates](puppet/iteration_with_index_in_templates.md)
- [Lookup Package Version](puppet/lookup_package_version.md)
- [Subscribe Repo Before Installing Packages](puppet/subscribe_repo_before_installing_packages.md)
- [Value Lookup with `hiera()`](puppet/value_lookup_with_hiera.md)

#### Python

- [Check Syntax](python/check_syntax.md)
- [Get Full Path of the Script](python/get_full_path_of_the_script.md)
- [Import from Parent Directory](python/import_from_parent_directory.md)
- [Install Latest Minor Version With `pip`](python/install_latest_minor_version_with_pip.md)
- [Install Dependencies Listed In File](python/install_dependencies_listed_in_file.md)
- [List Available Versions in `pip`](python/list_available_versions_in_pip.md)
- [Set Environment Variables When Activating _virtualenv_ With _virtualenvwrapper_](python/set_environment_variables_when_activating_virtualenv_with_virtualenvwrapper.md)

#### [Razor](https://github.com/puppetlabs/razor-server)

- [Create _Tag_ with numerical values](razor/create_tag_with_numerical_values.md)

#### Redis

- [Check if Redis is Alive](redis/check_if_redis_is_alive.md)

#### RegEx

- [RegEx Tools](regex/regex_tools.md)
  - [ReGex Testers](regex/regex_tools.md#regex-testers)
- [Strip ANSI Color Codes](regex/strip_ansi_color_codes.md)

#### MikroTik RouterOS

- [Block IP Address](routeros/block_ip_address.md)

#### Ruby

- [Check Syntax](ruby/check_syntax.md)
- [Packing/Unpacking _base64_](ruby/packing_base64.md)

#### Shell

- [`alias` With Parameters](shell/alias_with_parameters.md)
- [`AND`/`OR` Lists](shell/and_or_lists.md)
  - [Exit if Command Failed](shell/and_or_lists.md#exit-if-command-failed)
- [Bool Values](shell/bool_values.md)
- [Check if Running as _root_](shell/check_if_running_as_root.md)
- [Dot-Source a Script](shell/dot-source_a_script.md)
- [Extract Key Value from _json_](shell/extract_key_value_from_json.md)
- [Ignore Errors from Command](shell/ignore_errors_from_command.md)
- [Match String Against Regex](shell/match_string_against_regex.md)
- [One-Line `for` Loop](shell/one-line_for_loop.md)
- [Resolve Directory Name of the Script](shell/resolve_directory_name_of_the_script.md)
- [Run Command Under Directory](shell/run_command_under_directory.md)
- [Sectors to Kilobytes](shell/sectors_to_kilobytes.md)

#### Sublime Text 3

- [Collapse All Folders in Sidebar](sublime3/collapse_all_folders_in_sidebar.md)
- [Exclude Directory from Search](sublime3/exclude_directory_from_search.md)
- [Keyboard Shortcuts](sublime3/keyboard_shortcuts.md)
  - [Quick Switch Project](sublime3/keyboard_shortcuts.md#quick-switch-project)
  - [Refresh Folder List](sublime3/keyboard_shortcuts.md#refresh-folder-list)
  - [Show Unsaved Changes](sublime3/keyboard_shortcuts.md#show-unsaved-changes)

#### Subversion

- [Change Repository URL](svn/change_repository_url.md)
- [Force Commit Without Changes](svn/force_commit_without_changes.md)
- [Make File Executable](svn/make_file_executable.md)
- [Show Remote](svn/show_remote.md)

#### Vagrant

- [Create Base Box](vagrant/create_base_box.md)
  - [CentOS 7 on Virtualbox](vagrant/create_base_box.md#centos-7-on-virtualbox)
- [Minimal `Vagrantfile`](vagrant/minimal_vagrantfile.md)
- [Puppet Apply Execute](vagrant/puppet_apply_execute.md)

#### Vim

- [Delete All Lines](vim/delete_all_lines.md)

#### VirtualBox

- [Capture Network Packets](virtualbox/capture_network_packets.md)
- [List VMs](virtualbox/list_vms.md)
- [Start Headless VM](virtualbox/start_headless_vm.md)
- [Use Custom Lan Boot ROM](virtualbox/use_custom_lan_boot_rom.md)

#### Win

- [Change GTK Language](win/change_gtk_language.md)
- [Close _cmd_ Window After Running Batch File](win/close_cmd_window_after_running_batch_file.md)
- [Determine Number of Bits](win/determine_number_of_bits.md)
- [Disable CapsLock](win/disable_capslock.md)
- [Disable Typing Animation in Office 2013](win/disable_typing_animation_in_office_2013.md)
- [List Editions on Installation Media](win/list_editions_on_installation_media.md)
- [Restart _WSL_](win/restart_wsl.md)
- [Stop Windows Update Service](win/stop_windows_update_service.md)

#### Zabbix

- [Discovery Data Format](zabbix/discovery_data_format.md)

---

### About

I badgered idea for this from [jbranchaud/til](https://github.com/jbranchaud/til).

### License

&copy; 2016 Andrius Barkauskas

This repository is licensed under the MIT license. See [`LICENSE`](LICENSE) for details.
