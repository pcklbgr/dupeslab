### Keyboard Shortcuts

#### Quick Switch Project

Keyboard shortcut for _Quick Switch Project_ became disabled by default, to reenable - add following to user key bindings:

```
[
    {"keys": ["ctrl+alt+p"], "command": "prompt_select_workspace"}
]
```

---
- [Quick Project Switching](http://www.tutelagesystems.com/blog/view/sublime-text-3-quick-project-switching)


#### Refresh Folder List

```
[
    {"keys": ["ctrl+f5"], "command": "refresh_folder_list"}
]
```

---
- [Doesn’t refresh the project directory](https://forum.sublimetext.com/t/doesnt-refresh-the-project-directory/1583/8)

#### Show Unsaved Changes

```
[
    {"keys": ["ctrl+alt+d"], "command": "diff_changes"}
]
```

---
- [Shortcut key to show unsaved changes in Sublime Text 3](https://stackoverflow.com/a/30758468)
