### Exclude Directory from Search

To exclude directory add filter like so:

```
-*/name_of_dir/*
```

---
- [Exclude a directory from searching in Sublime Text 2/3](https://coderwall.com/p/bk90bw/exclude-a-directory-from-searching-in-sublime-text-2)
