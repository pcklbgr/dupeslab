### Collapse All Folders in Sidebar

`ctrl` + `alt` + `left click` on a node.

---
- [Collapse all expanded sidebar folders in Sublime Text 2](http://stackoverflow.com/a/28410642/462206)
