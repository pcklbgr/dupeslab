### Import from Parent Directory

```
srcdir
    subdir
        foo.py
    bar.py
```

To import `bar` in `foo` python `sys.path` can be updated.

In `foo.py`:

```python
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# import ../bar.py
import bar
```

---
- [Import file from parent directory?](http://stackoverflow.com/a/30536516/462206)
- [Import from sibling directory](http://stackoverflow.com/a/9806045/462206)
