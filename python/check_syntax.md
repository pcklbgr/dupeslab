### Check Syntax

Syntax of a python file can be verified by compiling it (this generates a _.pyc_ file):

```
python -m py_compile foo.py
```

Any syntax errors will be displayed:

```
$ python -m py_compile scripts/upgrade.py
Sorry: IndentationError: unexpected indent (upgrade.py, line 714)
```

---
- [How can I check the syntax of Python script without executing it?](http://stackoverflow.com/questions/4284313/how-can-i-check-the-syntax-of-python-script-without-executing-it/8437597#8437597)
