### Install Dependencies Listed In File

```
pip install -r requirements.txt
```

---
- [How to pip install packages according to requirements.txt from a local directory?](http://stackoverflow.com/a/15593865/462206)
- [Requirements Files](https://pip.pypa.io/en/stable/user_guide/#requirements-files)
- [Requirements File Format](https://pip.pypa.io/en/stable/reference/pip_install/#requirements-file-format)
