### List Available Versions in `pip`

Use `pip install` without specifying particular version:

```
pip install pymetar==
```

---
- [Python and pip, list all versions of a package that's available?](http://stackoverflow.com/a/26664162)
