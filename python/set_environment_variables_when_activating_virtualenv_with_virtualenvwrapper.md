### Set Environment Variables When Activating _virtualenv_ With _virtualenvwrapper_

Switch to _virtualenv_:

```bash
workon venv_foo
```

Update `$VIRTUAL_ENV/bin/postactivate` to export variables when _virtualenv_ is activated:

```bash
$ cat $VIRTUAL_ENV/bin/postactivate
#!/bin/bash
# This hook is sourced after this virtualenv is activated.

export PYTHONHTTPSVERIFY=0
```

Update `$VIRTUAL_ENV/bin/predeactivate` to unset exported variables when _virtualenv_ is deactivated:

```bash
$ cat $VIRTUAL_ENV/bin/predeactivate
#!/bin/bash
# This hook is sourced before this virtualenv is deactivated.

unset PYTHONHTTPSVERIFY
```

---
- [setting an environment variable in virtualenv](https://stackoverflow.com/a/11134336)
