### Install Latest Minor Version With `pip`

e.g. to install latest available Ansible _2.4_ minor version:

```
pip install 'ansible>=2.4,<2.4.99.99'
```

---
- `pip install` [Requirement Specifiers](https://pip.pypa.io/en/stable/reference/pip_install/#requirement-specifiers)
- [Pip: Specifying minor version](https://stackoverflow.com/a/6047897)
