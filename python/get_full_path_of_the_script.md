### Get Full Path of the Script

```python
import os
script_path = os.path.realpath(__file__)
```

Just directory name:


```python
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
```

---
- [How to properly determine current script directory?](https://stackoverflow.com/a/3718923)

