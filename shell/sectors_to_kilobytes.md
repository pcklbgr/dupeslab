### Sectors to Kilobytes

Converts (512 byte sized) sectors to kilobytes

```
echo $(((62924795*512)/1024))k
```
