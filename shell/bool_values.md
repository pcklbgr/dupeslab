### Bool Values

```
the_world_is_flat=true
# ...do something interesting...
if [ "$the_world_is_flat" = true ] ; then
    echo 'Be careful not to fall off!'
fi
```

---
- [How to declare and use boolean variables in shell script?](http://stackoverflow.com/a/2953673/462206)
