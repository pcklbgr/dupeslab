### Match String Against Regex

Bash operator `=~` can be used to match a string against regex:

```bash
if [[ $STRING =~ \{.*\} ]]; then
    # this is roughly a json
fi
```

---
- [Conditional Constructs](https://www.gnu.org/software/bash/manual/bashref.html#Conditional-Constructs)
- [Check if a string matches a regex in Bash script](https://stackoverflow.com/a/21112809)
