### Extract Key Value from _json_

To get value for _status_ key from json:

```
grep -Po '"status":.*?[^\\]",' status.json | perl -pe 's/"status"://; s/^"//; s/",$//'
```

---
- [Parsing JSON with Unix tools](https://stackoverflow.com/a/6852427)
