### `AND`/`OR` Lists

`AND` list:

```
command1 && command2
```

_command2_ is executed only if _command1_ returns exit status of **zero**

`OR` list:

```
command1 || command2
```

_command2_ is executed only if _command1_ returns exit status of **non-zero**

---
- [Lists of Commands](https://www.gnu.org/software/bash/manual/bashref.html#Lists)
- [Chaining together commands](http://tldp.org/LDP/abs/html/list-cons.html#LISTCONSREF)
- [Work the Shell - Understanding Shell Script Shorthand](http://www.linuxjournal.com/magazine/work-shell-understanding-shell-script-shorthand)


#### Exit if Command Failed

```
foo_command || { echo 'foo_command failed'; exit 1; }
```

---
- [http://stackoverflow.com/questions/3822621/how-to-exit-if-a-command-failed](http://stackoverflow.com/a/3822649/462206)
