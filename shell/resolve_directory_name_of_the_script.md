### Resolve Directory Name of the Script

Get full directory name where script is stored.

```bash
#!/usr/bin/env bash
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
```

---
- [Can a Bash script tell which directory it is stored in?](http://stackoverflow.com/a/246128/462206)
