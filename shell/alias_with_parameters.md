### `alias` With Parameters

Bash `alias` does not directly accept parameters, but it can point to a function that does.

```
copy_bak() {
    mv $1 $1.bak
    cp $2 $1
}
alias cpb=copy_bak
```

---
- [Make bash alias that takes parameter?](http://stackoverflow.com/a/7131683/462206)
- [Passing parameters to a bash function](http://stackoverflow.com/a/6212408/462206)
