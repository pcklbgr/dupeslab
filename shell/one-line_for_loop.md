### One-Line `for` Loop

Repeat command _n_ times:

```bash
for i in {1..5}; do echo $i; done
```

Iterate through files:

```bash
for f in *; do echo $f; done
```

---
- [HowTo: Use bash For Loop In One Line](http://www.cyberciti.biz/faq/linux-unix-bash-for-loop-one-line-command/)
