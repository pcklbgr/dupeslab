### Check if Running as _root_

```
if [ "$EUID" -ne 0 ]
  then echo "run as root"
  exit 1
fi
```
