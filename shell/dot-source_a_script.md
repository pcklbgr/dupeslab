### Dot-Source a Script

```
. /full/path/to/script
. ./relative/path/to/script
```

---
- [Source files in a bash script](http://stackoverflow.com/a/16011496/462206)
