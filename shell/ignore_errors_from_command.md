### Ignore Errors from Command

```
possibly_failing_script || true
```

---
- [Bash ignoring error for a particular command](https://stackoverflow.com/a/11231970)
