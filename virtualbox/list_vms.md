### List VMs

```
VBoxManage list vms
```

List only running VMs:

```
VBoxManage list runningvms
```

---
- [VBoxManage list](https://www.virtualbox.org/manual/ch08.html#vboxmanage-list)
