### Use Custom Lan Boot ROM

To use custom ROM for lan boot on a specific vm (_VM_NAME_):

```
VBoxManage setextradata VM_NAME VBoxInternal/Devices/pcbios/0/Config/LanBootRom /absolute/path/to/PXE-Intel.rom
```

Replace vm name with `global` to use ROM on all vms.
`PXE-Intel.rom` comes from _VirtualBox [Extension](https://www.virtualbox.org/wiki/Downloads) [Pack](https://www.virtualbox.org/manual/ch01.html#intro-installing)_

**NOTE:** VirtualBox limits lan boot ROM size to 57344 bytes. VM still boots if lan boot ROM is too big, error can be found in VMs log:

```
00:00:01.099828 PcBios: Failed to open LAN boot ROM file 'd:\code\razorsoccam\8086100e.rom', rc=VERR_TOO_MUCH_DATA!
```

**NOTE:** To check if custom lan boot ROM is already set use [`VBoxManage getextradata`](https://www.virtualbox.org/manual/ch08.html#idp46691721413248):

```
VBoxManage getextradata VM_NAME VBoxInternal/Devices/pcbios/0/Config/LanBootRom
```

---
- iPXE [git](https://git.ipxe.org/ipxe.git/blob/HEAD:/src/config/vbox/README)
- [Adding Intel PXE Boot Under FreeBSD/VirtualBox](https://grenville.wordpress.com/2012/03/18/adding-intel-pxe-boot-under-freebsdvirtualbox/)
- [robinsmidsrod/gist:4001104](https://gist.github.com/robinsmidsrod/4001104) _VirtualBox LAN boot ROM max size calculation_
