### Capture Network Packets

VirtualBox has built-in capability to capture network packets (creates _.pcap_ files which can be loaded to [Wireshark](https://www.wireshark.org/)).

```
VBoxManage modifyvm VMNAME --nictrace1 on --nictracefile1 TRACE.pcap
```

Give full and quoted path if running command from Cygwin shell.

---
- [Network tracing](https://www.virtualbox.org/wiki/Network_tips)
