### Start Headless VM

```
VBoxManage startvm $VM_NAME --type headless
```

---
- [VBoxManage startvm](https://www.virtualbox.org/manual/ch08.html#vboxmanage-startvm)
