### Run Queries from CLI

```
psql -U username -d database -c 'SELECT * FROM table'
```

---
- [Run postgresql queries from the command line](http://stackoverflow.com/a/21859632)
