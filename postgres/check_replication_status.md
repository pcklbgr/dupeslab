### Check Replication Status

Replication state on _master_.

```sql
select client_addr, state, sent_location, write_location, flush_location, replay_location from pg_stat_replication;
```

Replication delay on _slave_.

```sql
select now() - pg_last_xact_replay_timestamp() AS replication_delay;
```

---
- [Show Replication Status in Postgresql](https://www.niwi.nz/2013/02/16/replication-status-in-postgresql/)
