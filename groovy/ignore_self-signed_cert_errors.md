### Ignore Self-signed Cert Errors

<span style="color:red">**NOTE:** this disables certificate validation altogether, do not use in untrusted networks (i.e. public interwebs).</span>

Ignore self-signed certificate errors (overrides certificate validation):

```
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

def nullTrustManager = [
    checkClientTrusted: { chain, authType ->  },
    checkServerTrusted: { chain, authType ->  },
    getAcceptedIssuers: { null }
]

def nullHostnameVerifier = [
    verify: { hostname, session -> true }
]

SSLContext sc = SSLContext.getInstance("SSL")
sc.init(null, [nullTrustManager as X509TrustManager] as TrustManager[], null)
HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
HttpsURLConnection.setDefaultHostnameVerifier(nullHostnameVerifier as HostnameVerifier)
```

---
- [How to use SSL with a self-signed certificate in groovy?](https://stackoverflow.com/a/3242379)
- [Grails javax.net.ssl.SSLHandshakeException: java.security.cert.CertificateException No name matching](http://groovy-grails.blogspot.lt/2009/07/javaxnetsslsslhandshakeexception.html)
