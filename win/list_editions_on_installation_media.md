### List Editions on Installation Media

To list editions available on installation media (in other words list images on a _.wim_ file) `Dism` (_Deployment Image Servicing and Management_) tool can be used.
_.wim_ file on Windows .iso are located in _source_ directory (or _x86\sources_ and _x64\sources_ if .iso has 32bit and 64bit setup files).

Run `Dism` agains _.wim_ file to list available editions:

```cmd
 Dism /Get-WimInfo /WimFile:"c:\offline\install.wim"
```

```cmd
C:\Program Files (x86)\Windows Kits\8.1\Assessment and Deployment Kit\Deployment Tools>dism /get-wiminfo /wimfile:"d:\soft\MS Windows Server 2012 R2\install.wim"

Deployment Image Servicing and Management tool
Version: 6.3.9600.17029

Details for image : d:\soft\MS Windows Server 2012 R2\install.wim

Index : 1
Name : Windows Server 2012 R2 SERVERSTANDARDCORE
Description : Windows Server 2012 R2 SERVERSTANDARDCORE
Size : 6.898.373.863 bytes

Index : 2
Name : Windows Server 2012 R2 SERVERSTANDARD
Description : Windows Server 2012 R2 SERVERSTANDARD
Size : 12.051.460.352 bytes

Index : 3
Name : Windows Server 2012 R2 SERVERDATACENTERCORE
Description : Windows Server 2012 R2 SERVERDATACENTERCORE
Size : 6.871.511.192 bytes

Index : 4
Name : Windows Server 2012 R2 SERVERDATACENTER
Description : Windows Server 2012 R2 SERVERDATACENTER
Size : 12.065.366.117 bytes

The operation completed successfully.
```

To get more information on specific edition use `/Index`:

```cmd
Dism /Get-WimInfo /WimFile:"c:\offline\install.wim" /Index:1
```

```cmd
C:\Program Files (x86)\Windows Kits\8.1\Assessment and Deployment Kit\Deployment Tools>dism /get-wiminfo /wimfile:"d:\soft\MS Windows Server 2012 R2\install.wim" /index:2

Deployment Image Servicing and Management tool
Version: 6.3.9600.17029

Details for image : d:\soft\MS Windows Server 2012 R2\install.wim

Index : 2
Name : Windows Server 2012 R2 SERVERSTANDARD
Description : Windows Server 2012 R2 SERVERSTANDARD
Size : 12.051.460.352 bytes
WIM Bootable : No
Architecture : x64
Hal : acpiapic
Version : 6.3.9600
ServicePack Build : 17031
ServicePack Level : 0
Edition : ServerStandardEval
Installation : Server
ProductType : ServerNT
ProductSuite : Terminal Server
System Root : WINDOWS
Directories : 19342
Files : 89400
Created : 2014.03.21 - 23:40:33
Modified : 2014.03.21 - 23:41:07
Languages : en-US (Default)

The operation completed successfully.
```

---
- `Dism` [Command-Line Options](https://technet.microsoft.com/en-us/library/dd744382(v=ws.10).aspx)
- [Q. How do I get WIM information using the Deployment Image Servicing and Management (DISM) tool?](http://windowsitpro.com/systems-management/q-how-do-i-get-wim-information-using-deployment-image-servicing-and-management-di)
- [How to see which build and edition of Windows 10 the iso file contains](http://winaero.com/blog/how-to-see-which-build-and-edition-of-windows-10-the-iso-file-contains/)
