if($args.Count -eq 0) {
  "provide a file name or path to file";exit
}

if((test-path -path $args) -ne $true) {
  "file doesnt seem to exist" ; exit
}

$fs = New-Object IO.Filestream($args , [Io.FileMode]::Open)
$br = New-Object IO.BinaryReader($fs)

if($br.Readchar()-ne'M') {
  "no mz";exit
}
if($br.Readchar()-ne'Z') {
  "no mz";exit
}

$fs.Seek(0x3c,[IO.SeekOrigin]::Begin) | Out-Null
$elfaw_new = $br.ReadUInt32();
$peheader=$fs.Seek($elfaw_new,[IO.SeekOrigin]::Begin)

if($br.Readchar()-ne'P') {
  "no pe";exit
}
if($br.Readchar()-ne'E') {
  "no pe";exit
}

$mctypeoff = $fs.seek($peheader+4,[IO.SeekOrigin]::Begin)
$mctype= $br.ReadUInt16()

switch($mctype) {
  0x0000 { "{0:x4} {1}" -f  $mctype , "Unknown machine type"}
  0x01d3 { "{0:x4} {1}" -f  $mctype , "Matsushita AM33"}
  0x8664 { "{0:x4} {1}" -f  $mctype , "x64"}
  0x01c0 { "{0:x4} {1}" -f  $mctype , "ARM little endian"}
  0x01c4 { "{0:x4} {1}" -f  $mctype , "ARMv7 (or higher) Thumb mode only"}
  0xaa64 { "{0:x4} {1}" -f  $mctype , "ARMv8 in 64-bit mode"}
  0x0ebc { "{0:x4} {1}" -f  $mctype , "EFI byte code"}
  0x014c { "{0:x4} {1}" -f  $mctype , "Intel 386 or later family processors"}
  0x0200 { "{0:x4} {1}" -f  $mctype , "Intel Itanium processor family"}
  0x9041 { "{0:x4} {1}" -f  $mctype , "Mitsubishi M32R little endian"}
  0x0266 { "{0:x4} {1}" -f  $mctype , "MIPS16"}
  0x0366 { "{0:x4} {1}" -f  $mctype , "MIPS with FPU"}
  0x0466 { "{0:x4} {1}" -f  $mctype , "MIPS16 with FPU"}
  0x01f0 { "{0:x4} {1}" -f  $mctype , "Power PC little endian"}
  0x01f1 { "{0:x4} {1}" -f  $mctype , "Power PC with floating point support"}
  0x0166 { "{0:x4} {1}" -f  $mctype , "MIPS little endian"}
  0x01a2 { "{0:x4} {1}" -f  $mctype , "Hitachi SH3"}
  0x01a3 { "{0:x4} {1}" -f  $mctype , "Hitachi SH3 DSP"}
  0x01a6 { "{0:x4} {1}" -f  $mctype , "Hitachi SH4"}
  0x01a8 { "{0:x4} {1}" -f  $mctype , "Hitachi SH5"}
  0x01c2 { "{0:x4} {1}" -f  $mctype , "ARM or Thumb (`“interworking`”)"}
  0x0169 { "{0:x4} {1}" -f  $mctype , "MIPS little-endian WCE v2"}
};$fs.close()
