### Close _cmd_ Window After Running Batch File

_cmd_ window will close afterwards if `start` is used to execute commands.

```
start "" .\RimWorld1284Win.exe -savedatafolder=save
```

---
- [`START`](http://ss64.com/nt/start.html)
- [How to close the command line window after running a batch file?](http://stackoverflow.com/questions/14626178/how-to-close-the-command-line-window-after-running-a-batch-file/14626301#14626301)
