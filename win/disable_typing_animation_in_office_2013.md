### Disable Typing Animation in Office 2013

Navigate to `HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Common\Graphics` (create key `Graphics` if it does not exist).

Add _DWOR (32-bit) Value_ `DisableAnimations` with data value of `1`.

Restart Office applications.

--
- [How to Disable the Typing Animation in Office 2016 or Office 2013](http://www.laptopmag.com/articles/office-2013-typing-animation-disable)
