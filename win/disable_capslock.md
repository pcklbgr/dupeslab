### Disable CapsLock

Disable `CapsLock` altogether ([no_caps.reg](no_caps.reg)):

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Keyboard Layout]
"Scancode Map"=hex:00,00,00,00,00,00,00,00,02,00,00,00,00,00,3a,00,00,00,00,00
```

Turn `CapsLock` to `Esc` ([caps_to_esc.reg](caps_to_esc.reg)):

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Keyboard Layout]
"Scancode Map"=hex:00,00,00,00,00,00,00,00,03,00,00,00,3a,00,46,00,01,00,3a,00,00,00,00,00
```

Turn `CapsLock` to `Ctrl` ([caps_to_ctrl.reg](caps_to_ctrl.reg)):

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Keyboard Layout]
"Scancode Map"=hex:00,00,00,00,00,00,00,00,02,00,00,00,1d,00,3a,00,00,00,00,00 
```

Turn `CapsLock` to `Win ⊞` ([caps_to_win.reg](caps_to_win.reg)):

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Keyboard Layout]
"Scancode Map"=hex:00,00,00,00,00,00,00,00,02,00,00,00,5b,e0,3a,00,00,00,00,00
```

**NOTE:** these require reboot.

--
- [Disable caps lock key](https://answers.microsoft.com/en-us/windows/forum/windows_7-desktop/disable-caps-lock-key/8083d4ee-721d-463f-aeda-630cd7f047c3)
- [Map caps lock to escape in Windows](https://vim.fandom.com/wiki/Map_caps_lock_to_escape_in_Windows#Registry)
- [Disable Caps Lock (or turn it into something else)](http://johnhaller.com/useful-stuff/disable-caps-lock)
- [howto://map caps lock to windows-key](http://retrohack.com/howto-map-caps-lock-to-windows-key/)
