### Determine Number of Bits

Open binary file in _notepad_ and look for first occurrence of `PE`.

_x86_ has `PE  L`

_x64_ has `PE  d†`

**NOTE:** this is not the best way to do this, _notepad_ does not like big files.

PowerShell  script [`bintest.ps1`](bintest.ps1) can give more detail (taken from [here](http://reverseengineering.stackexchange.com/a/11373)).

---
- [How to check if a binary is 32 or 64 bit on Windows?](http://superuser.com/a/889267)
- [Check if exe is 64-bit](http://reverseengineering.stackexchange.com/a/6041)
