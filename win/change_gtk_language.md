### Change GTK Language

By default applications written using GTK autodetect language from locale settings.
To prevent that use a simple .bat file to launch executable.

```
@echo off

set LANG=en
start gitg.exe
```

[`start`](http://ss64.com/nt/start.html) ensures that cmd window is closed as soon as executable starts.

---
- [Change the language to english?](http://forum.deluge-torrent.org/viewtopic.php?t=37193#p156551)
- [Change interface language in auto-detecting GTK apps](https://www.rarst.net/software/change-interface-language/)
