### Stop Windows Update Service

To stop service run from command line (as administrator):

```
net stop wuauserv
```

To start service (who would want that..):

```
net start wuauserv
```

---
- [Start or stop Windows update service](http://www.windows-commandline.com/start-stop-windows-update-service/)
