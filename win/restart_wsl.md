### Restart _WSL_

To restart/reboot WSL run in Powershell (admin):

```powershell
Restart-Service LxssManager
```

---
- [Rebooting Ubuntu on Windows without rebooting Windows?](https://superuser.com/a/1347725)
