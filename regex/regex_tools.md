### ReGex Tools

#### ReGex Testers

- [RegExr](http://regexr.com/) - javascript flavor regex tester.
- [Rubular](http://rubular.com/) - ruby flavor regex tester.
- [regular expressions 101](https://regex101.com/) - pcre (php), javascript, python flavor regex tester.
- [Debuggex](https://www.debuggex.com/) - javascript, python, pcre (php) flavor regex tester.
