### Strip ANSI Color Codes

`sed` expression to strip ANSI color codes:

```
sed 's/\x1B\[[0-9;]*[JKmsu]//g' -i foo
```

---
- [Removing Color Codes From Output](http://unix.stackexchange.com/a/55547)
