### Check Storage Engine in Use

In MongoDB shell:

```
db.serverStatus().storageEngine
```

Via MongoDB cli:

```
mongo -u admin -p 'password' --eval 'printjson(db.serverStatus().storageEngine)' admin
```

---
- [check storage engine from shell](http://dba.stackexchange.com/questions/96917/check-storage-engine-from-shell/107689#107689)
