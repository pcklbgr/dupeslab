### Query Secondary Replica Set Members

Use `rs.slaveOk()` in `mongo` shell to be able to query secondary replica set members.

---
- [How to Query MongoDB Secondary Replica Set Members](http://3t.io/blog/how-to-query-mongodb-secondary-replica-set-members/)
