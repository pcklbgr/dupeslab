### Evaluate Command

MongoDB shell commands can be evaluated with `--eval` flag for `mongo` client:

```
mongo --eval "printjson(db.serverStatus())" admin
```

---
- [How to execute mongo commands through shell scripts?](http://stackoverflow.com/questions/4837673/how-to-execute-mongo-commands-through-shell-scripts/6000360#6000360)
