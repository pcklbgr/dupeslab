### Get List of Files in Repo

Query to get list of files in the repo (_AppRepo_):

```json
{
  "action":"coreui_Component",
  "method":"readAssets",
  "data":[
    {
      "page":1,
      "start":0,
      "limit":300,
      "sort":[
        {
          "property":"name",
          "direction":"ASC"
        }
      ],
      "filter":[
        {
          "property":"repositoryName",
          "value":"AppRepo"
        }
      ]
    }
  ],
  "type":"rpc",
  "tid":15
}
```

e.g. using `curl`:

```
curl -u admin:admin 'https://localhost:8081/service/extdirect' -H 'Content-Type: application/json' -H 'Accept: */*' --data-binary '{"action":"coreui_Component","method":"readAssets","data":[{"page":1,"start":0,"limit":300,"sort":[{"property":"name","direction":"ASC"}],"filter":[{"property":"repositoryName","value":"AppRepo"}]}],"type":"rpc","tid":15}' --compressed
```

---
- [nexus 3: get list of all uploaded files to raw repository](https://stackoverflow.com/a/44077340)
