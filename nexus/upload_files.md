### Upload Files

Files can be uploaded via _HTTP PUT_ e.g. using `curl`:

```
curl -v -u admin:admin --upload-file artifact.tar.gz http://localhost:8081/repository/artifacts/artifact-1.0.tar.gz
```

---
- [How can I programmatically upload an artifact into Nexus 3?](https://support.sonatype.com/hc/en-us/articles/115006744008-How-can-I-programmatically-upload-an-artifact-into-Nexus-3-)
