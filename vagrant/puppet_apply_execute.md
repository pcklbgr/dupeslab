### Puppet Apply Execute

Puppet apply execute (`puppet apply -e`) can be done with Vagrants shell provisioner.

```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell",
    inline: "puppet apply -e 'package { 'mc': ensure => installed, }'"
end
```

---
- [Man Page: puppet apply](https://docs.puppet.com/puppet/3.8/reference/man/apply.html)
- [Adhoc Puppetry with puppet apply execute](http://www.puppetcookbook.com/posts/simple-adhoc-execution-with-apply-execute.html)
- [Shell Provisioner](https://www.vagrantup.com/docs/provisioning/shell.html)
