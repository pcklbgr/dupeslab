### Create Base Box

#### CentOS 7 on Virtualbox

**NOTE:** VirtualBox Guest Additions from version 4.3.x does not work with CentOS 7. Version 5.0.x and up works just fine.

- [Download](https://www.centos.org/download/) CentOS 7 minimal .iso.

- Install CentOS 7 on vm:
  - Set password for root to `vagrant`;
  - Create user `vagrant` (with password `vagrant`);

- Enable wired network, `/etc/sysconfig/network-scripts/ifcfg-eth0` (interface name might be [different](https://wiki.centos.org/FAQ/CentOS7#head-62d45421abea0220e3038796e3dd5315906fa493) e.g. _enp0s3_) should contain lines:

```
ONBOOT="yes"
BOOTPROTO="dhcp"
```

- Set on all netwrok interfaces (this prevents `/etc/resolv.conf` from getting overriden):

```
PEERDNS="no"
NM_CONTROLLED="no"
```

- Stop/disable _Network Manager_ & _iptables_ services:

```
systemctl disable NetworkManager.service && systemctl stop NetworkManager.service
systemctl disable firewalld && systemctl stop firewalld
```

- Install and enable openssh:

```
yum -y install openssh-server
systemctl start sshd.service
systemctl enable sshd.service
```

- Forward ssh port in virtualbox (vm needs to be stopped) on host:

```
VBoxManage modifyvm "cent7" --natpf1 "guestssh,tcp,,2222,,22"
```

- Install packages required to build VirtualBox Guest Additions (`dkms` is from EPEL).

```
yum install epel-release
yum install gcc kernel-devel kernel-headers dkms make bzip2 perl`

```

- Mount Guest Additions .iso and run setup script (_Devices_ > _Insert Guest Additions CD Image_ on vm window):

```
mkdir /mnt/tmp
mount /dev/cdrom /mnt/tmp/
sh /mnt/tmp/VBoxLinuxAdditions.run
umount /mnt/tmp/
rm -rf /mnt/tmp/
```

- Enable sudo for _vagrant_ user:

```
echo 'Defaults:vagrant !requiretty' > /etc/sudoers.d/vagrant
echo 'vagrant ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/vagrant
chmod 440 /etc/sudoers.d/vagrant
```

- Add _vagrant_ user to _admin_ group:

```
groupadd admin
usermod -G admin vagrant
```

- Add Vagrant ssh keys:

```
mkdir -pm 700 /home/vagrant/.ssh
wget --no-check-certificate 'https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub' -O /home/vagrant/.ssh/authorized_keys
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant /home/vagrant/.ssh
```

- Mark box build date:

```
date > /etc/vagrant_box_build_time
```

- Update Installed packages:

```
yum update
```

- Clean Yum cache:

```
yum clean all
 rm -rf /var/cache/yum
```

- Package Vagrant box (vm has to be stopped):

```
vagrant package --base cent7 --output centos7.box
```

- Add box to Vagrant:

```
vagrant box add centos7 centos7.box
```
---
- [Creating a Base Box](https://www.vagrantup.com/docs/boxes/base.html) in Vagrant [documentation](https://www.vagrantup.com/docs/)
- [How to Create a CentOS Vagrant Base Box](https://github.com/ckan/ckan/wiki/How-to-Create-a-CentOS-Vagrant-Base-Box) on [ckan/ckan](https://github.com/ckan/ckan)
- [vagrant.sh](https://github.com/RackHD/RackHD/blob/master/packer/scripts/vagrant.sh) on [RackHD/RackHD](https://github.com/RackHD/RackHD)
