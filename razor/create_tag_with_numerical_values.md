### Create _Tag_ with Numerical Values

```
razor create-tag --name small --rule '["=", ["fact", "processorcount"], 1]'
```

_NOTE:_ razor client help (`razor create-tag -h`) has example that uses rule `["=", ["fact", "processorcount"], "2"]` that would not really work as `"2"` is a string value and [code](https://github.com/puppetlabs/razor-server/blob/master/lib/razor/matcher.rb#L138) does not check if compared values are of same type.

[Getting started](https://github.com/puppetlabs/razor-server/wiki/Getting-started#creating-basic-objects) on git gives a working example.

There's a full list of operators in [source code](https://github.com/puppetlabs/razor-server/blob/master/lib/razor/matcher.rb).
