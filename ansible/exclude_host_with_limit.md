### Exclude Host With `--limit`

```
ansible-playbook playbooks/playbook.yml --limit 'all:!not_on_this_one'
```

---
- [Limiting Playbook/Task Runs](https://ansible-tips-and-tricks.readthedocs.io/en/latest/ansible/commands/#limiting-playbooktask-runs)
