### List All `ansible_` Variables

```
ansible -m setup HOSTNAME
```

---
- [How do I see a list of all of the ansible_ variables?](http://docs.ansible.com/ansible/faq.html#how-do-i-see-a-list-of-all-of-the-ansible-variables)
