### Unsafe Strings

Use `!unsafe` to prevent character substitution in vars:

```yaml
---
version_cmd: !unsafe 'rpm -qa --qf "%|EPOCH?{%{EPOCH}}:{0}|:%{NAME}-%{VERSION}-%{RELEASE}.*\n" rabbitmq-server'
```

Without `!unsafe` above example would fail with _failed at splitting arguments, either an unbalanced jinja2 block or quotes_ error if passed down to e.g. `shell`.

---
- [Unsafe or Raw Strings](https://docs.ansible.com/ansible/latest/user_guide/playbooks_advanced_syntax.html#unsafe-or-raw-strings)
