### Reboot Machine and Wait for It to Come Back Up

Using `reboot` module (added in version _2.7_):

```yaml
- name: reboot the machine
  reboot:
```

In older versions:

```yaml
tasks:
  - name: reboot
    shell: "shutdown -r +1"
    async: 0
    poll: 0
    ignore_errors: true
  - name: wait for machine to boot up
    wait_for:
      host: "{{ (ansible_ssh_host|default(ansible_host))|default(inventory_hostname) }}"
      port: 22
      search_regex: 'OpenSSH'
      delay: 30
      timeout: 300
    connection: local
    become: false
```

---
- [`reboot`](https://docs.ansible.com/ansible/latest/modules/reboot_module.html)
- [`wait_for`](https://docs.ansible.com/ansible/2.6/modules/wait_for_module.html)
