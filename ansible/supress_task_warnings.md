### Supress Task Warnings

Add `warn=false` to a task (e.g. to supress _[WARNING]: Consider using yum module rather than running yum_):

```yaml
tasks:
  - shell: 'yum clean all'
    args:
      warn: false
```

---
- per task silencing of warnings [#13468](https://github.com/ansible/ansible/issues/13468)
- [Avoid Ansible command warning](https://ansibledaily.com/avoid-ansible-command-warnings/)
