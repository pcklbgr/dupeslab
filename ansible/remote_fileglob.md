### Remote Fileglob

To _glob_ and install loose .rpm files:

```yaml
- name: glob rpms
  shell: "ls /path/to/rms/*.rpm"
  register: rpm_files

- name: install rpms
  yum:
    name: "{{ item }}"
    state: present
  with_items: "{{ rpm_files.stdout_lines }}"
```

---
- [Is there with_fileglob that works remotely in ansible?](https://stackoverflow.com/a/33544316)
