### Escape Quotes in Variable

Use `>` (block scalar) to define variable. `>-` will not include trailing newline.

```yaml
secret: >-
  this'is"a_nasty_quoted_string
secret_too: >
  this'is"another_quoted_string-and'a"newline
```

---
- YAML spec on [Scalars](http://www.yaml.org/spec/1.2/spec.html#id2760844)
- [How to escape double and single quotes in YAML within the same string](https://stackoverflow.com/a/37432679)
