### Config

#### Override Config Value On the Fly

To override configuration value on the fly use `$ANSIBLE_CONFIG_VALUE` environment variable.

e.g. to override `roles_path = /home/foo/ansible/roles` in `/etc/ansible/ansible.cfg` to use directory `roles` from current directory:

```
ANSIBLE_ROLES_PATH=roles ansible-playbook playbooks/apache.yml
```

---
- [Configuration file](http://docs.ansible.com/ansible/intro_configuration.html)

#### Formatted Error Output

Set `stdout_callback` to _debug_ in `ansible.cfg`:

```ini
[defaults]
stdout_callback=debug
```

Alternativelly - set `ANSIBLE_STDOUT_CALLBACK` environment variable to _debug_.

_Note:_ this works with Ansible version _2.4_ and up.

---
- [Clean error output in ansible-playbook](https://stackoverflow.com/a/45086602)
- [Callback Plugins](https://docs.ansible.com/ansible/2.5/plugins/callback.html#plugin-list)

#### Disable _.retry_ Files

Set `retry_files_enabled` to _false_ (_true_ by default) in `ansible.cfg` (under `[defaults]`):

```ini
[defaults]
retry_files_enabled = False
```

---
- [RETRY_FILES_ENABLED](https://docs.ansible.com/ansible/2.5/reference_appendices/config.html#retry-files-enabled)
- [How do you stop Ansible from creating .retry files in the home directory?](https://stackoverflow.com/a/31318882)
