### Include Tasks from a File

Use `include`, e.g.:

```yaml
- include: redhat.yml
  when: ansible_os_family == "RedHat"
```

In Ansible _2.4_ & up - use `import_tasks`, e.g.:

```yaml
- import_tasks: redhat.yml
  when: ansible_os_family == "RedHat"
```

---
- [include - Include a play or task list](https://docs.ansible.com/ansible/2.5/modules/include_module.html)
- [import_tasks - import a task list](https://docs.ansible.com/ansible/2.4/import_tasks_module.html)
