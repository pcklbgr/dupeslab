### Force Handlers to Run

use `meta: flush_handlers`:

```yaml
tasks:
  - shell: "echo 'this is a task'"
  - meta: flush_handlers
  - shell: "echo 'this is also a task'"
```

---
- [Handlers: Running Operations On Change](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#handlers-running-operations-on-change)
- [How to force handler to run before executing a task in Ansible?](https://stackoverflow.com/a/34020073)
