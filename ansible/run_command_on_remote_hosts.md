### Run Command on Remote Hosts

```
ansible target_hosts -m shell -a 'echo $PATH'
```

If elevated privileges are needed - use `--become` (`--ask-become-password`):

```
ansible target_hosts -m shell -a 'tail /var/log/messages' --become --ask-become-password
```

---
- [Parallelism and Shell Commands](http://docs.ansible.com/ansible/latest/intro_adhoc.html#parallelism-and-shell-commands)
