### Conditional Role

Role `compile_php` will only be included if `do_compile_php: true`

```yaml
- hosts: all
  roles:
    - role: yum
    - { role: compile_php, when: do_compile_php }
```

---
- [Ansible: Conditional role dependencies](http://edunham.net/2015/09/10/ansible_conditional_role_dependencies.html)
