### _SSL: CERTIFICATE_VERIFY_FAILED_ Error

```
export PYTHONHTTPSVERIFY=0
```

---
- [SSL: CERTIFICATE_VERIFY_FAILED #66](https://github.com/dj-wasabi/ansible-zabbix-agent/issues/66)
