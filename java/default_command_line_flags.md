### Default Command Line Flags

To see default JRE command line flags (and version):

```
java -XX:+PrintCommandLineFlags -version
```

---
- [find which type of garbage collector is running](https://stackoverflow.com/a/9980041)
