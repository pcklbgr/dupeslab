### Get Build Console Output

Last build console output:

```
curl "$JENKINS_URL/job/$JOB_NAME/lastBuild/consoleText"
```

Specific build console output:

```
curl "$JENKINS_URL/job/$JOB_NAME/$BUIL_NUMBER/consoleText"
```

---
- [How to get Jenkins “Console Output” after triggering build remotely?](http://stackoverflow.com/a/24176303/462206)
