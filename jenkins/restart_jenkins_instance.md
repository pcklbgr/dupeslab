### Restart Jenkins Instance

Jenkins can be restarted via _Script Console_.

Performs restart:

```java
Hudson.instance.restart()
```

Performs a safe restart (restarts when no builds are running):

```java
Hudson.instance.safeRestart()
```

---
- Jenkins main module 2.17 API [`restart()`](http://javadoc.jenkins-ci.org/jenkins/model/Jenkins.html#restart--)
- Jenkins main module 2.17 API [`safeRestart`](http://javadoc.jenkins-ci.org/jenkins/model/Jenkins.html#safeRestart--)
