### Clear Job Build History

Run in _Script Console_

```groovy
def job = Jenkins.instance.getItem("Job Name")
job.getBuilds().each { it.delete() }
job.save()
```

---
- [Clear Jenkins build history ( clear build yesterday )](https://superuser.com/a/1418896)
