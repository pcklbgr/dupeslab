### Disable Security

Stop Jenkins and set `useSecurity` to false in `$JENKINS_HOME/config.xml`:

```xml
<useSecurity>false</useSecurity>
```

Setting `useSecurity` effectively wipes `authorizationStrategy` & `securityRealm`, back up `$JENKINS_HOME/config.xml` to keep values.

---
- [Disabling Security](https://jenkins.io/doc/book/system-administration/security/#disabling-security)
- [How to reset Jenkins security settings from the command line?](https://stackoverflow.com/a/20388835)
