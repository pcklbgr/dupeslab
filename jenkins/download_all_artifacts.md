### Download All Artifacts

Download artifacts from last successfull build of project _McProjectyFace_ under _rpm_ directory as `rpm.zip`
(any name can be used):

```
curl -LO "http://jenkins.com/job/McProjectyFace/lastSuccessfulBuild/artifact/rpm/*zip*/rpm.zip"
```

---
- [Downloading Artifacts from Jenkins using wget or curl](http://stackoverflow.com/questions/31432189/downloading-artifacts-from-jenkins-using-wget-or-curl/31434010#31434010)
