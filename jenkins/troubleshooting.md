### Troubleshooting

#### Error on Accessing Internal Git Repo: _no matching key exchange method found. Their offer: diffie-hellman-group1-sha1_

Old-timey key exchange method `diffie-hellman-group1-sha1` needs to be enabled (OpenSSH does not enable it by default).

Add to `~/.ssh/config` file:

```
Host repo.host.org
    KexAlgorithms +diffie-hellman-group1-sha1
```

--
- [OpenSSH Legacy Options](http://www.openssh.com/legacy.html)
