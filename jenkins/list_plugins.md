### List Plugins

```
println(Jenkins.instance.pluginManager.plugins)
```

One per line:

```
Jenkins.instance.pluginManager.plugins.each {
  println(it)
}
```
