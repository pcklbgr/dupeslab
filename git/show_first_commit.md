### Show First Commit

```
git rev-list --max-parents=0 HEAD
```

**NOTE:** `--max-parents` is awailable in versions _1.7.4.2_ (or newer).

---
- [How to show first commit by 'git log'?](https://stackoverflow.com/a/5189296)
- [How to reference the initial commit?](https://stackoverflow.com/a/1007545)
