### History of a Range of Lines

Use `git log -L<line>,<range>:<filename>`, e.g.:

```
git log -L 20,24:foo.file
```

---
- [Retrieve the commit log for a specific line in a file?](http://stackoverflow.com/questions/8435343/retrieve-the-commit-log-for-a-specific-line-in-a-file/19757493#19757493)
- [Git: discover which commits ever touched a range of lines](http://stackoverflow.com/questions/14142609/git-discover-which-commits-ever-touched-a-range-of-lines/19757441#19757441)
- [git-log](https://git-scm.com/docs/git-log)
