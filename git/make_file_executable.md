### Make File Executable

To make file executable (or in general change file mode) - use `git update-index` (`git add` first if it is a new file):

```
git update-index --chmod=+x foo.sh
```

Check file mode with `git ls-files`:

```
git ls-files --stage
```

List _only_ executable files:

```
git ls-files --stage | grep 100755
```

---
- [git-update-index](https://www.git-scm.com/docs/git-update-index/)
- [git-ls-files](https://www.git-scm.com/docs/git-ls-files/)
- [How to create file execute mode permissions in Git on Windows?](http://stackoverflow.com/questions/21691202/how-to-create-file-execute-mode-permissions-in-git-on-windows/21694391#21694391)
- [How to read the mode field of git-ls-tree's output](http://stackoverflow.com/questions/737673/how-to-read-the-mode-field-of-git-ls-trees-output/8347325#8347325)
- [index-format.txt](https://github.com/git/git/blob/master/Documentation/technical/index-format.txt) on [git/git](https://github.com/git/git)
