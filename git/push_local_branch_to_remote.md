### Push Local Branch to Remote

To push an (new) local branch to remote repsitory:

```bash
git push --set-upstream origin BRANCH_FOO
```

> `-u`  
> `--set-upstream`  
> For every branch that is up to date or successfully pushed, add upstream (tracking) reference, used by argument-less _git-pull_ and other commands. 

---
-[git-push](https://git-scm.com/docs/git-push)
-[Push a new local branch to a remote Git repository and track it too](http://stackoverflow.com/questions/2765421/push-a-new-local-branch-to-a-remote-git-repository-and-track-it-too/6232535#6232535)
