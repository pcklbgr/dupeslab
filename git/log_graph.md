### Log Graph

To get commit log graph use (add `--all` to show all branches):

```bash
git log --decorate --graph --oneline
```

There are ways to get [better](http://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git/34467298#34467298) [looking](https://git.wiki.kernel.org/index.php/Aliases#Getting_pretty_logs) output.
There's also nifty tools to get [pretty](http://stackoverflow.com/questions/1057564/pretty-git-branch-graphs/9074343#24107223) [graphics](http://stackoverflow.com/questions/1057564/pretty-git-branch-graphs/9074343#25468472) for commit graphs.

---
- [Visualizing branch topology in git](http://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git)
- [Pretty git branch graphs](http://stackoverflow.com/questions/1057564/pretty-git-branch-graphs)
