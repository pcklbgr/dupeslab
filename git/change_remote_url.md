### Change Remote URL

```
git remote set-url REMOTE git@github.com:USERNAME/OTHERREPOSITORY.git
```

---
- [Changing a remote's URL](https://help.github.com/articles/changing-a-remote-s-url/)
