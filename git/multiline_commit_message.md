### Multiline Commit Message

Multiline commit message can be done with bash _here document_

```
$ git commit -F- <<EOF
> Here
> goes
> message
> EOF
```

---
- [Here Documents](http://tldp.org/LDP/abs/html/here-docs.html)
- [Add line break to git commit -m from command line](http://stackoverflow.com/a/5064653/462206)
