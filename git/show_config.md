### Show Config

```
git config --list
```

Look at `~/.gitconfig`.
Local config is in repository's `.git/config` file.

---
- [git-config](https://git-scm.com/docs/git-config)
- [Show git config](http://stackoverflow.com/a/12254105)
