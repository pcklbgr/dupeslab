### History of Moved File

Use `--follow` option with `git log`:

```
git log --follow file.txt
```

---
- [Viewing GIT history of moved files](https://stackoverflow.com/a/3845243)
- [git-log](https://git-scm.com/docs/git-log)
