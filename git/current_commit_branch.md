### Current Commit/Branch

Current commit hash:

```
git rev-parse HEAD
```

To show commit hashes for submodules:

```
git submodule foreach git rev-parse HEAD
```

Current branch:

```
git rev-parse --abbrev-ref HEAD
```

---
- [How to retrieve the hash for the current commit in Git?](http://stackoverflow.com/questions/949314/how-to-retrieve-the-hash-for-the-current-commit-in-git/949391#949391)
- [How to get the current branch name in Git?](https://stackoverflow.com/a/12142066)
