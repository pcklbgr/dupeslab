### Apply Patch

`git apply` applies the patch

```
git apply patch_file.diff
```

---
- [git-apply](https://git-scm.com/docs/git-apply)
