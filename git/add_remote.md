### Add Remote

Use `git remote add REMOTE_NAME REMOTE_URL`:

```
git remote add gitlab git@gitlab.com:pcklbgr/dupeslab.git
```

---
- [Git Basics - Working with Remotes](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes)
