### Remove File from Checkin

Revert one checkin back:

```
git reset --soft HEAD~1
```

Reset unwanted files (leave them out of commit):

```
git reset HEAD path/to/file.foo
```

Commit again (with same message):

```
git commit -c ORIG_HEAD
```

---
- [Remove files from Git commit](http://stackoverflow.com/questions/12481639/remove-files-from-git-commit/15321456#15321456)
