### Handy Aliases

[kernel.org](https://www.kernel.org/) wiki has extensive [list](https://git.wiki.kernel.org/index.php/Aliases) of aliases.

##### Log Commits from last week made by "current" user

```
week = !git log --author=\"`git config user.name`\" --since=1.weeks
```

---
- [Log Last Week](log_last_week.md)
- [Using Git Log to Show Last Month's Commits](http://blog.element84.com/using-git-log-to-show-last-months-commits.html)


##### Commit Log Graph

```
lg = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative
```

---
- [Getting pretty logs](https://git.wiki.kernel.org/index.php/Aliases#Getting_pretty_logs)
