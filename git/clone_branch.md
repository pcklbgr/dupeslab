### Clone Branch

Use `--branch <name>` (`-b <name>`) to clone a specific branch (cloned repo will point to `<name>` instead of _HEAD_).

```
git clone -b stable/liberty https://git.openstack.org/openstack-dev/devstack
```

---
- [git-log](https://git-scm.com/docs/git-clone)
