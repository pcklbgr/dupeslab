### Colorized Output

Add section `[color]` in your `~/.gitconfig`:

```ini
[color]
  diff = auto
  status = auto
  branch = auto
  interactive = auto
  ui = true
  pager = true
```

---
- [How to colorize output of git?](https://unix.stackexchange.com/a/44283)
