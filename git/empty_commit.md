### Empty Commit

Created a commit without any files.

```
git commit --allow-empty -m "I feel empty inside"
```

---
- [git-commit](https://git-scm.com/docs/git-commit)
- [git commit --allow-empty](https://coderwall.com/p/vkdekq/git-commit-allow-empty)
