### Disable Advices

Disables "how to stage/unstage/add" hints:

```
git config --global advice.statusHints false
```

---
- git config [variables](https://git-scm.com/docs/git-config#_variables)
- [A few of my Git tricks, tips and workflows](http://nuclearsquid.com/writings/git-tricks-tips-workflows/)
