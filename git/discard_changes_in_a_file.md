### Discard Changes in a File

To discard any changes made in file:

```
git checkout -- FILE.foo
```

---
- Undoing Things - [Unmodifying a Modified File](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things#Unmodifying-a-Modified-File)
