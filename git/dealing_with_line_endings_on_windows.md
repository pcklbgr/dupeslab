### Dealing with Line Endings on Windows

Set `core.autocrlf` to `false`:

```
git config --global core.autocrlf false
```

Set `core.eol` to `lf`:

```
git config --global core.eol lf
```

Per-repo setting can be changed via `.gitattributes` or by setting local `core.eol`.

---
- [How do I force git to use LF instead of CR+LF under windows?](http://stackoverflow.com/a/13154031)
