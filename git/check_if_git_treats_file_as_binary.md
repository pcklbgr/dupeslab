### Check if `git` Treats File as Binary

```
git diff --no-index --numstat /dev/null path/to/binary/file.bin
```

For _binary_ files `--numstat` output should start with `-  - ` (that is `- TAB - TAB`).

---
- [How to determine if Git handles a file as binary or as text?](http://stackoverflow.com/a/6134127/462206)
