### Add Remote to Fresh Repository

Initialize the repository:

```
git init
```

Add the remote:

```
git remote add origin git@gitlab.com:pcklbgr/dupeslab.git
```

Pull from specific remote (`origin` is the remote in this example):

```
git pull origin master
```

Make local _master_ branch track changes from the remote:

```
git branch --set-upstream-to=origin/master master
```
