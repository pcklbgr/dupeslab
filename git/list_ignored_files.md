### List Ignored Files

To list files ignored by git (i.e. files that match patterns in `.gitignore`)

```
git status --ignored
```

---
- [Show ignored files in git](http://stackoverflow.com/questions/466764/show-ignored-files-in-git/467053#467053)
