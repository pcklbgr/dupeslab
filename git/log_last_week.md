### Log Last Week

Git log can show a subset of commits with limits based on time with options `--since` and `--until`.
To get list of commits made in last week:

```bash
git log --since=1.weeks
```

---
- Viewing the Commit History - [Limiting Log Output](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History#Limiting-Log-Output)
- [git-log](https://git-scm.com/docs/git-log)
