### Unpushed Changes

To list files that are not yet in remote (e.g. _origin_):

```bash
git diff --stat origin/master
```

To list commits that are not yet in remote:

```bash
git log origin/master..HEAD
```

---
- [How can I check which commits have not been pushed to origin?](http://stackoverflow.com/questions/4082175/how-can-i-check-which-commits-have-not-been-pushed-to-origin/4082762#4082762)
