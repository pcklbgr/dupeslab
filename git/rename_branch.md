### Rename Branch

To rename local branch:

```
git branch -m OLDNAME NEWNAME
```

To rename current branch:

```
git branch -m NEWNAME
```

---
- [git-branch](https://git-scm.com/docs/git-branch)
- [How do I rename a local Git branch?](http://stackoverflow.com/a/6591218)
