### Unstage Staged File

Trere are [more than one](http://stackoverflow.com/questions/6919121/why-are-there-2-ways-to-unstage-a-file-in-git) way to do that.
[`git reset`](https://git-scm.com/docs/git-reset) seem to do the trick:

```
git reset -- foo.txt
```

---
- [Git tricks: Unstaging files](http://www.andrewberls.com/blog/post/git-tricks-unstaging-files)
