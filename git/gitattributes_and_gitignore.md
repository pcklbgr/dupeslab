### `.gitattributes` and `.gitignore`


#### Negate Ignore Pattern

Pattern with a prefix `!` negates any previous pattern, e.g.

```
*.rom
!rom/*.rom
```

---
- Pattern Format on [_gitignore_](https://git-scm.com/docs/gitignore)


#### Treat Files as Binary

To treat file as binary add to `.gitattributes`:

```
*.rom binary
```

---
- Git Attributes - [Binary Files](https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes#Binary-Files)
