### List Untracked Files

```
git ls-files --others --exclude-standard
```

---
- [Git: list only “untracked” files (also, custom commands)](https://stackoverflow.com/a/3801554)
