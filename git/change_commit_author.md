### Change Commit Author

To change author of the latest commit (committer will will remain set to configured user in `user.name` and `user.email`) use

```
git commit --amend --author "New Foo <foo@bar.baz>"
```

To set committter to something specific use

```
git -c user.name="New Foo" -c user.email=foo@bar.baz commit --amend --reset-author
```

Use `--no-edit` to skip opening the editor altogether.

---
- [Change the author of a commit in Git](http://stackoverflow.com/questions/750172/change-the-author-of-a-commit-in-git/1320317#1320317)


#### Change Commit Author by Rewriting History

This will change **ALL** commits to have new (same) author and committer.

```
git filter-branch -f --env-filter "GIT_AUTHOR_NAME='New Name'; GIT_AUTHOR_EMAIL='foo@bar.baz'; GIT_COMMITTER_NAME='New Name'; GIT_COMMITTER_EMAIL='foo@bar.baz';" HEAD
```

---
- [Change the author of a commit in Git](http://stackoverflow.com/questions/750172/change-the-author-of-a-commit-in-git/750191#750191)
