### Submodules

Clone a repo with submodules:

```
git clone --recursive REPO
```

Checkout submodules on existing repo:

```
git submodule update --init
```
