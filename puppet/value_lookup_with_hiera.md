### Value Lookup with `hiera()`

> The `hiera()` lookup function performs a Hiera lookup, using its input as the lookup key. The result of the lookup must be a string; any other result will cause an error.

```
wordpress::database_server: "%{hiera('instances::mysql::public_hostname')}"
```

---
- [The `hiera()` Lookup Function](https://docs.puppet.com/hiera/3.1/variables.html#the-hiera-lookup-function)
