### Subscribe Repo Before Installing Packages

Add following line to puppet manifest to ensure that (yum) repos are subscribed before any packages are installed.

```puppet
Yumrepo <| |> -> Package <| provider != 'rpm' |>
```

---
- [Adding a yum repo to puppet before doing anything else](http://serverfault.com/a/461869)
- [Language: Resource Collectors](https://docs.puppet.com/puppet/3.8/reference/lang_collectors.html)
