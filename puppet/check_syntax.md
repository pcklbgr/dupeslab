### Check Syntax

```
puppet parser validate init.pp
```

---
- [Man Page: puppet parser](https://docs.puppet.com/puppet/3.8/man/parser.html)
