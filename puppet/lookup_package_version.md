### Lookup Package Version

To lookup package version that can be used to install specific version e.g.:

```puppet
package { 'nginx':
  ensure  => '1.10.1-1.el6.ngx',
}
```

For `.rpm` packages:

```
rpm -q nginx --qf '%{VERSION}-%{RELEASE}\n'
```
