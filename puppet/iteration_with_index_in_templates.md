### Iteration with Index in Templates

```ruby
<% @values.each_with_index do | val, index | -%>
Some stuff with <%= val %> and <%= index %>
<% end -%>
```

---
- ruby 1.8.7 [Enumerable](http://ruby-doc.org/core-1.8.7/Enumerable.html#method-i-each_with_index)
