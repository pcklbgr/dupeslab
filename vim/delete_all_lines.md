### Delete All Lines

- `gg` - to go to beginning of file;
- `dG` - delete everything to the end-of-file

---
- [How to delete all lines of file in Vim](https://codeyarns.com/2011/04/18/how-to-delete-all-lines-of-file-in-vim/)
