### Discovery Data Format

A JSON with hashes inside an array, e.g.:

```json
{
    "data": [
    {
        "{#NODENAME}": "node1",
        "{#NODEINTERFACE}": "eth0"
    },
    {
        "{#NODENAME}": "node2",
        "{#NODEINTERFACE}": "eth3"
    }
]
}
```

---
- [Creating custom LLD rules](https://www.zabbix.com/documentation/3.4/manual/discovery/low_level_discovery#creating_custom_lld_rules)
- [Zabbix Discovery JSON Formatting](https://serverfault.com/a/857813)
