### Check if User Account is Locked/Password Expired

```
Get-ADUser $USERNAME -Properties LockedOut,PasswordExpired | Select-Object LockedOut,PasswordExpired
```

**NOTE:** this requires [Active Directory Module](./active_directory_module.md)

---
- [How can I verify if an AD account is locked?](https://stackoverflow.com/a/27043150)
