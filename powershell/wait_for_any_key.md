### Wait for _Any Key_

`[Console]::ReadKey()` can be used to pause PowerShell script until user hits any key:

```powershell
function Pause {
    Write-Host -NoNewLine "Smash any key to continue.."
    [Console]::ReadKey($true) | Out-Null
    Write-Host
}
```

This function works like DOS [`pause`](http://ss64.com/nt/pause.html) command.
