### Network info

##### IP Address

```powershell
(gwmi Win32_NetworkAdapterConfiguration | ? { $_.IPAddress -ne $null }).ipaddress
```

Get _'first'_ IPv4 address:

```powershell
((ipconfig | findstr [0-9].\.)[0]).Split()[-1]
```

---
- [how to retrieve your ip address with powershell...](http://marcusoh.blogspot.lt/2008/04/how-to-retrieve-your-ip-address-with.html)

##### Hostname and FQDN

Get hostname:

```powershell
[System.Net.Dns]::GetHostEntry([string]$env:computername).HostName
```

Get FQDN:

```powershell
(Get-WmiObject win32_computersystem).DNSHostName+"."+(Get-WmiObject win32_computersystem).Domain
```

---
- [Powershell: Get FQDN Hostname](http://stackoverflow.com/questions/12268885/powershell-get-fqdn-hostname/23632876#23632876)
