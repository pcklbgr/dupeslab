### Active Directory Module

Download and install _Remote Server Administration Tools for Windows 7 with Service Pack 1 (SP1)_ [KB958830](https://www.microsoft.com/en-us/download/details.aspx?id=7887)

Enable _Active Directory Module for Windows PowerShell_ feature.

![Active Directory Module](active_directory_module.png)

Run to import Active Directory module in PowerShell:

```
Import-Module ActiveDirectory
```

---
- [Active Directory Cmdlets in Windows PowerShell](https://technet.microsoft.com/en-us/library/ee617195.aspx)
- [Description of Remote Server Administration Tools for Windows 7](https://support.microsoft.com/en-us/help/958830/description-of-remote-server-administration-tools-for-windows-7)
