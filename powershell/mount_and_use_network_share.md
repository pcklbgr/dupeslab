### Mount and Use Network Share

Starting with PowerShell 3.0 [`New-PSDrive`](https://technet.microsoft.com/en-us/library/hh849829.aspx) has parameter `-Persist` that makes share mount usable throughout the script.

`net use` can be utilized to mount and use network share:

```powershell
net use j: \\server\share

# makes sure that mounted location can be used from script
Set-Location FILESYSTEM::J:

# unmounts share (/yes unmounts share without user prompt)
net use j: /delete /yes
```

---
- [Net use](https://technet.microsoft.com/en-us/library/bb490717.aspx)
- [NET USE only works once in powershell](http://stackoverflow.com/questions/10994979/net-use-only-works-once-in-powershell)
- [Why “net use * /delete” does not work but waits for confirmation in my PowerShell script?](http://stackoverflow.com/questions/13284106/why-net-use-delete-does-not-work-but-waits-for-confirmation-in-my-powershel/13284418#13284418)
