### Run Script

By default PowerShell has lots of restrictions that prevent simply _running_ scripts.
To run scripts with least restrictions use `-ExecutionPolicy` parameters

```powershell
PowerShell.exe -NoProfile -ExecutionPolicy Bypass .\foo.ps1
```

`-NoProfile` prevents PowerShell from loading any scripts from user profile.

---
- [PowerShell.exe Command-Line Help](https://msdn.microsoft.com/en-us/powershell/scripting/core-powershell/console/powershell.exe-command-line-help)
- [about_Execution_Policies](https://technet.microsoft.com/library/hh847748.aspx)
- [How to enable execution of PowerShell scripts?](http://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts/533745#533745)
