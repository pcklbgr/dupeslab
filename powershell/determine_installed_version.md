### Determine PowerShell Version

Installed PowerShell version can be determined with [`Get-Host`](https://technet.microsoft.com/en-us/library/hh849946.aspx) or `$PSVersionTable`.
`$PSVersionTable` was introduced in PowerShell _2.0_ it will not work in _1.0_.

```powershell
PS C:\> get-host


Name             : ConsoleHost
Version          : 4.0
InstanceId       : dd4cedcb-6f45-42fe-b345-ca2daed19694
UI               : System.Management.Automation.Internal.Host.InternalHostUserInterface
CurrentCulture   : lt-LT
CurrentUICulture : en-US
PrivateData      : Microsoft.PowerShell.ConsoleHost+ConsoleColorProxy
IsRunspacePushed : False
Runspace         : System.Management.Automation.Runspaces.LocalRunspace



PS C:\> $PSVersionTable

Name                           Value
----                           -----
PSVersion                      4.0
WSManStackVersion              3.0
SerializationVersion           1.1.0.1
CLRVersion                     4.0.30319.34209
BuildVersion                   6.3.9600.16406
PSCompatibleVersions           {1.0, 2.0, 3.0, 4.0}
PSRemotingProtocolVersion      2.2
```

---
- [Determine installed PowerShell version](http://stackoverflow.com/questions/1825585/determine-installed-powershell-version/2092948#2092948)
