### Exit Code of `Start-Process`

Getting exit code of `(Start-Process notepad.exe).ExitCode` is not that [easy](http://stackoverflow.com/questions/10262231/obtaining-exitcode-using-start-process-and-waitforexit-instead-of-wait).

`$LASTEXITCODE` stores exit code of the last executable execution.

```powershell
Start-Process notepad.exe
Write-Host $LASTEXITCODE
```

---
- [%Errorlevel%](http://ss64.com/nt/errorlevel.html)
- [Difference between $? and $LastExitCode in PowerShell](http://stackoverflow.com/questions/10666035/difference-between-and-lastexitcode-in-powershell)
