### Get Directory Name of the Script

PowerShell 3 (and later) has automatic variable `$PSScriptRoot`

In older versions query `MyInvocation.MyCommand.Definition` property:

```powershell
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
```

---
- [What's the best way to determine the location of the current PowerShell script?](http://stackoverflow.com/a/5466355/462206)
- [about_Automatic_Variables](https://technet.microsoft.com/en-us/library/hh847768.aspx)
