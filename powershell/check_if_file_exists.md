### Check if File Exists

`-PathType leaf` - last element of `-Path` is a file

```powershell
Test-Path -Path path\to\file -PathType leaf
```

---
- [`Test-Path`](https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.management/test-path)
