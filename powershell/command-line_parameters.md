### Command-line Parameters

Command-line parameters are defined in `Param` section at the beggining (first executable line) of file.

```powershell
param (
    [string]$user,
    [string]$server = "http://defaultserver",
)
```

This adds parameters `-user` and `-server` (with a default value) to script.

Positional parameters:

```powershell
[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True, Position=0)]
    [string]$vmList
)
```

---
- [How to handle command-line arguments in PowerShell](http://stackoverflow.com/a/2157625/462206)
- [Script Parameters / Arguments](http://ss64.com/ps/syntax-args.html)
- [Making PowerShell Params](http://windowsitpro.com/blog/making-powershell-params)
