### Catch-all HTTP to HTTPS Redirect

```
server {
    listen 80 default_server;
    server_name !@#;
    return 301 https://$host$request_uri;
}
```

---
- [Redirect HTTP to HTTPS in Nginx](https://serversforhackers.com/c/redirect-http-to-https-nginx)
- [In Nginx, how can I rewrite all http requests to https while maintaining sub-domain?](https://serverfault.com/a/337893)
- Server names - [Miscellaneous names](http://nginx.org/en/docs/http/server_names.html#miscellaneous_names)
- Pitfalls and Common Mistakes - [Taxing Rewrites](https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/#taxing-rewrites)
