### List Array Numerically

Use numerical comparison operator `<=>`:

```
@sorted = sort { $a <=> $b } @unsorted;
```

---
- [`sort`](http://perldoc.perl.org/functions/sort.html)
- [Sorting an Array Numerically](https://www.safaribooksonline.com/library/view/perl-cookbook/1565922433/ch04s15.html)
