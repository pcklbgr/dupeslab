### Convert Between UNIX and Windows Paths

Use `cygpath --windows` to convert from unix to windows path.

```
$ cygpath -w /cygdrive/d/tmp/
D:\tmp\
```

Use `cygpath --unix` to convert from windows path to unix path.
Bash interprets `\` as an escape character, it needs to be typed twice.

```
$ cygpath -u D:\\tmp\\
/cygdrive/d/tmp/
```

---
- [How do I convert between Windows and UNIX paths?](https://cygwin.com/faq/faq.html#faq.using.converting-paths)
- [`cygpath`](https://cygwin.com/cygwin-ug-net/cygpath.html)
