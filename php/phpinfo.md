### `phpinfo`

Create .php file with `<?php phpinfo();?>` inside:

```
echo "<?php phpinfo();?>" > info.php
```

Or directly with php cli:

```
php <<< '<?php phpinfo();?>'
```

---
- [`phpinfo`](http://php.net/manual/en/function.phpinfo.php)
