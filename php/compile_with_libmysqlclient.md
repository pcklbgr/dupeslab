### Compile with `libmysqlclient`

PHP [documentation](http://php.net/manual/en/mysqlinfo.library.choosing.php#mysqlinfo.library.choosing) lies, to compile PHP (_5.6_) with `libmysqlclient` configure like so:

```
./configure --with-libdir=lib64 --with-mysqli=/usr/bin/mysql_config --with-pdo-mysql=/usr --with-mysql=/usr
```

Install `mysql-libs` and `mysql-devel` packages (`mysql_config` is from `mysql-devel`).
