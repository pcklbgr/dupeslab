### _nohup_ Running Process

- `ctrl` + `z` to pause process;
- `bg` to resume process;
- `disown -h $JOBNUBMER`. `$JOBNUMER` (e.g. `%1`) can be determined by running `jobs`.

---
- [Job Control](http://web.mit.edu/gnu/doc/html/features_5.html)
- [How do I put an already-running process under nohup?](https://stackoverflow.com/a/625436)
- [How to keep processes running after ending ssh session?](https://askubuntu.com/a/222855)
