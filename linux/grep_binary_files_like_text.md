## `grep` Binary Files Like Text

Use `-a` (`--text`) option or `--binary-files=text ` option.

```sh
grep -a 'foo' /textish/binary/file
```
Another way is to pipe through `tr` to trim any `NUL` characters:

```sh
cat /textish/binary/file | tr -d '\000' | grep 'foo'
```

---
- `grep` [manpage](https://man7.org/linux/man-pages/man1/grep.1.html)
- [What makes grep consider a file to be binary?](https://unix.stackexchange.com/questions/19907/what-makes-grep-consider-a-file-to-be-binary)
