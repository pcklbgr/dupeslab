### `deb` Packages

#### Find Package That Provides File

`dpkg -S` can be used to find _installed_ package that owns a file:

```
dpkg -S /bin/ls
```

To look through packages available in repositories - use `apt-file`.

Install `apt-file` and generate package database:

```
aptitude install apt-file
apt-file update
```

Search for package:

```
apt-file search libmp3lame.so.0
```

---
- `dpkg` [manpage](http://manpages.ubuntu.com/manpages/trusty/man1/dpkg.1.html)
- `apt-file` on Debian [wiki](https://wiki.debian.org/apt-file)
- [How do I find the package that provides a file?](http://askubuntu.com/a/482)
- [Debian & Ubuntu Equivalents of ‘yum whatprovides’](http://nicholaskuechler.com/2011/02/10/debian-ubuntu-equivalents-of-yum-whatprovides/)


#### List Available Package Versions for `apt`

Lists versions available for installation with `apt` (Aptitude)

```
apt-cache policy PACKAGE
```

Output will contain [priority](http://unix.stackexchange.com/questions/121413/understanding-the-output-of-apt-cache-policy/121419#121419) [numbers](http://askubuntu.com/questions/282602/what-do-the-numbers-in-the-output-of-apt-cache-policy-tell-us/282610#282610).
According to `apt_preferences` [man page](http://linux.die.net/man/5/apt_preferences):

```
If the target release has been specified then APT uses the following algorithm to set the priorities of the versions of a package. Assign:

priority 1
    to the versions coming from archives which in their Release files are marked as "NotAutomatic: yes" but not as "ButAutomaticUpgrades: yes" like the Debian experimental archive.

priority 100
    to the version that is already installed (if any) and to the versions coming from archives which in their Release files are marked as "NotAutomatic: yes" and "ButAutomaticUpgrades: yes" like the Debian backports archive
    since squeeze-backports.

priority 500
    to the versions that are not installed and do not belong to the target release.

priority 990
    to the versions that are not installed and belong to the target release.
```

---
- [How to find out which versions of a package can I install on APT](http://superuser.com/questions/393681/how-to-find-out-which-versions-of-a-package-can-i-install-on-apt/945406#945406)


#### Restore Configuration Files

```
apt-get -o Dpkg::Options::="--force-confmiss" install --reinstall PACKAGE
```

Use [`dpkg -S`](find_package_that_provides_file.md#debian-packages) to find which package installed config file.

---
- [How can I restore configuration files?](http://askubuntu.com/a/67028)


#### List Packages Available for Upgrade

```
apt-get -s -u upgrade
```

---
- `apt-get` [manpage](https://linux.die.net/man/8/apt-get)
- [List available updates but do not install them](https://unix.stackexchange.com/a/19472)
