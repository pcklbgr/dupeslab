### Generate _"Random"_ MAC Address

Generate MAC address tied to FQDN:

```sh
echo $(hostname -f)|md5sum|sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/'
```

---
- [how to generate a random MAC address from the Linux command line](https://serverfault.com/a/299563)
