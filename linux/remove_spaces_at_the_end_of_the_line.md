### Remove Spaces at the End of the Line

To remove spaces only:

```
sed -i 's/ *$//' file
```

To remove both spaces and tabs:

```
sed -i 's/[[:blank:]]*$//' file
```

This does in-place edit, use `-i.bak` to create backup in file _file.bak_

---
- [remove white space from the end of line in linux](http://stackoverflow.com/questions/20521857/remove-white-space-from-the-end-of-line-in-linux/20521915#20521915)
