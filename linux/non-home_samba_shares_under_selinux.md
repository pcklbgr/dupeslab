### Non-home Samba Shares Under SELinux

To share (non-home) directory with Samba set `samba_export_all_ro` boolean to allow read-only access to standard directories
(this allows Samba to read every file on the system):

```
setsebool -P samba_export_all_ro=1
```

To allow read-write access - set `samba_export_all_rw`:

```
setsebool -P samba_export_all_rw=1
```

Wihtout booleans set attempts to connect to share with `smbclient //server/share` will result in error _tree connect failed: NT_STATUS_BAD_NETWORK_NAME_ even if shares are listed with `smbclient -L //server`.

---
- [Sharing non-home directory files](https://selinuxproject.org/page/SambaRecipes#Sharing_non-home_directory_files)
- _Step 3_ on [Setting Up Samba and Configure FirewallD and SELinux to Allow File Sharing on Linux/Windows Clients](http://www.tecmint.com/setup-samba-file-sharing-for-linux-windows-clients/)
- `setsebool` [manpage](http://man7.org/linux/man-pages/man8/setsebool.8.html)
