### View SELinux Context

```
ls -Z /path/to/FOO
```
---
- [SELinux Contexts](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Security-Enhanced_Linux/chap-Security-Enhanced_Linux-SELinux_Contexts.html)
