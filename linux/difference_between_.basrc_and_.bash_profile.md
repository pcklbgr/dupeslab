### Difference Between `.bashrc` and `.bash_profile`

`.bash_profile` is executed for login shells, while `.bashrc` is executed for interactive non-login shells.

---
- `bash` [manpage](http://man7.org/linux/man-pages/man1/bash.1.html#FILES)
- [What's the difference between .bashrc, .bash_profile, and .environment?](http://stackoverflow.com/a/415444)
