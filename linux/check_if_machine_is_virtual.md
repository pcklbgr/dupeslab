### Check if Machine is Virtual

Check `/proc/cpuinfo` for `hypervisor` flag:

```
grep hypervisor /proc/cpuinfo
```

Check `dmidecode` output for _Manufacturer_/_Product_:

```
dmidecode | grep -i 'manufacturer\|product'
```

Check `dmesg` output for _Hypervisor_:

```
dmesg | grep -i hypervisor
```

Use `virt-what` if available.


---
- [Find out if the OS is running in a virtual environment](https://unix.stackexchange.com/questions/3685/find-out-if-the-os-is-running-in-a-virtual-environment)
