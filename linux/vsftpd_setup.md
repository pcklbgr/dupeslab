### _vsftpd_ Setup

#### Debian Wheezy

Install _vsftpd_, _apache2-utils_ (for `htpasswd`), [PAM](https://en.wikipedia.org/wiki/Pluggable_authentication_module) and _lftp_ (for [FTP-SSL](https://en.wikipedia.org/wiki/FTPS)):

```
aptitude install vsftpd apache2-utils libpam-pwdfile lftp
```

Create virtual user for _vsftpd_ (to add more users ommit `-c` flag for `openssl`):

```
mkdir -p /etc/vsftpd
htpasswd -c -p -b /etc/vsftpd/ftpd.passwd USERNAME $(openssl passwd -1 -noverify USERNAME_PASSWORD)
```

Create private directories for virtual user:

```
mkdir -p /srv/ftp/USERNAME
chown ftp:ftp /srv/ftp/USERNAME/
chmod 555 /srv/ftp/USERNAME/
```

Update PAM config:

```
cp /etc/pam.d/vsftpd /etc/pam.d/vsftpd.bak
cat > /etc/pam.d/vsftpd <<- _EOF
auth required pam_pwdfile.so pwdfile /etc/vsftpd/ftpd.passwd
account required pam_permit.so
_EOF
```

Generate SSL certificate for _vsftpd_

```
openssl req -x509 -nodes -days 7300 -newkey rsa:2048 -keyout /etc/ssl/private/vsftpd.pem -out /etc/ssl/private/vsftpd.pem
chmod 600 /etc/ssl/private/vsftpd.pem
```

Configure service (`/etc/vsftpd.conf`):

```
cp /etc/vsftpd.conf /etc/vsftpd.conf.bak
cat > /etc/vsftpd.conf <<- _EOF
# Run standalone?  vsftpd can run either from an inetd or as a standalone
# daemon started from an initscript.
listen=YES

# Make sure PORT transfer connections originate from port 20 (ftp-data).
connect_from_port_20=YES

# Use this option to override the IP address that vsftpd will advertise in response to the PASV command.
pasv_address=88.222.93.97

# If vsftpd is in standalone mode, this is the port it will listen on for incoming FTP connections.
listen_port=11401

# The minimum and maximum ports to allocate for PASV style data connections.
pasv_min_port=11300
pasv_max_port=11400

# Maximum number of clients which may be connected.
max_clients=20

# Activate logging of uploads/downloads.
xferlog_enable=YES

# Activate directory messages - messages given to remote users when they
# go into a certain directory.
dirmessage_enable=YES

# You may fully customise the login banner string:
ftpd_banner=vault

# Allow anonymous FTP? (Beware - allowed by default if you comment this out).
anonymous_enable=NO

# Uncomment this to allow local users to log in.
local_enable=YES

# Uncomment this to enable any form of FTP write command.
write_enable=YES

# Default umask for local users is 077. You may wish to change this to 022,
# if your users expect that (022 is used by most other ftpd's)
local_umask=002

# You may restrict local users to their home directories.  See the FAQ for
# the possible risks in this before using chroot_local_user or
# chroot_list_enable below.
chroot_local_user=YES

# If enabled, all user and group information in directory
# listings will be displayed as "ftp".
hide_ids=YES

# This string is the name of the PAM service vsftpd will use.
pam_service_name=vsftpd

# If enabled, all non-anonymous logins are classed as "guest" logins.
# A guest login is remapped to the user specified in the guest_username setting.
guest_enable=YES

# This setting is the real username which guest users are mapped to.
guest_username=ftp

# This is the name of the user that is used by vsftpd when it wants to be totally unprivileged.
nopriv_user=ftp

# Directory which vsftpd will try to change into after a local (i.e. non-anonymous) login.
local_root=/srv/ftp/$USER

# Automatically generate a home directory for each virtual user, based on a template.
user_sub_token=$USER

# If enabled, virtual users will use the same privileges as local users.
virtual_use_local_privs=YES

# This option should be the name of a directory which is empty.  Also, the
# directory should not be writable by the ftp user. This directory is used
# as a secure chroot() jail at times vsftpd does not require filesystem
# access.
secure_chroot_dir=/var/run/vsftpd/empty

# If enabled, and vsftpd was compiled against OpenSSL, vsftpd will support secure connections via SSL.
ssl_enable=YES

# If enabled, this option will permit TLS v1 protocol connections. TLS v1 connections are preferred.
ssl_tlsv1=YES

# If activated, all non-anonymous logins are forced to use a secure SSL connection in order to send the password.
force_local_logins_ssl=YES

# This option specifies the location of the RSA certificate to use for SSL
# encrypted connections.
rsa_cert_file=/etc/ssl/private/vsftpd.pem
rsa_private_key_file=/etc/ssl/private/vsftpd.pem
_EOF
```

Restart _vsftpd_ service:

```
service vsftpd restart
```

Check if _vsftpd_ works (`ls` in ftp should throw no errors):

```
lftp -u USERNAME,USERNAME_PASSWORD localhost
```

Create shared directory:

```
mkdir /mnt/store/shared
chmod 775 /mnt/store/shared
chown ftp:users /mnt/store/shared
```

Create mountpoint for shared directory:

```
mkdir /srv/ftp/USERNAME/shared
chown ftp:ftp /srv/ftp/USERNAME/shared
```

Add `fstab` mount bind entry:

```
# binds for vsftpd (ftpdave virtual user)
/mnt/store/shared    /srv/ftp/USERNAME/shared    none    bind    0    0
```

Mount shared folder:

```
mount /srv/ftp/USERNAME/shared
```

---
- _vsftpd_ Debian [wiki](https://wiki.debian.org/vsftpd)
- [`vsftpd.conf`](http://vsftpd.beasts.org/vsftpd_conf.html)
- [How to setup virtual users for vsftpd with access to a specific sub directory?](http://askubuntu.com/a/575825)
- _vsftpd_ Ubuntu [documentation](https://help.ubuntu.com/community/vsftpd)
- _Very Secure FTP Daemon_ Arch Linux [wiki](https://wiki.archlinux.org/index.php/Very_Secure_FTP_Daemon)
- _`vsftpd` Configuration Options_ RHEL 3 [Reference Guide](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/3/html/Reference_Guide/s1-ftp-vsftpd-conf.html)
- [vsFTPd and Symbolic Links](https://radu.cotescu.com/vsftpd-and-symbolic-links/)
- `htpasswd` [manpage](https://httpd.apache.org/docs/current/programs/htpasswd.html)
- `openssl` [manpage](https://www.openssl.org/docs/manmaster/apps/openssl.html)
- `lftp` [manpage](http://lftp.yar.ru/lftp-man.html)
