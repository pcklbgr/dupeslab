### Generate `udev` `70-persistent-net.rules`

To (re)generate `udev` `/etc/udev/rules.d/70-persistent-net.rules` file:

```
udevadm trigger --subsystem-match=net
```

---
- [Manually trigger creation of /etc/udev/rules.d/70-persistent-net.rules file](https://access.redhat.com/discussions/1240213)
