### Mount DFS share

Add following to `/etc/request-key.conf` (comes with `keyutils` package on RH/CentOS):

```
create   dns_resolver   *   *   /usr/sbin/cifs.upcall   %k
```

Mount via `fstab` or `mount` as `cifs`, e.g.:

```
\\dfs.host\share   /mnt/dfs_share  cifs   _netdev,rw   0   0
```

---
- `mount.cifs` [man page](https://linux.die.net/man/8/mount.cifs)
- [How to Mount a DFS Share in Linux](https://mikemstech.blogspot.com/2012/10/how-to-mount-dfs-share-in-linux.html)
