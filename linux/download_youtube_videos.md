### Download Youtube Videos

[rg3/youtube-dl](https://github.com/rg3/youtube-dl) can download videos from Youtube listed in a file.

```
youtube-dl -f bestaudio --autonumber-size 3 --restrict-filenames --output "%(autonumber)s-%(upload_date)s-%(title)s-%(id)s.%(ext)s" --batch-file list
```
