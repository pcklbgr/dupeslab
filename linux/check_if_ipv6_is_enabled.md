### Check if IPv6 is Enabled

Check if `/proc/net/if_inet6` exists:

```
test -f /proc/net/if_inet6 && echo "IPv6" || echo "no IPv6"
```

Check `sysctl` output.
Values for `net.ipv6.conf.all.disable_ipv6` and `net.ipv6.conf.default.disable_ipv6` should be _0_ if IPv6 is enabled.

```
/sbin/sysctl -a | grep -e '[all|default]\.disable_ipv6'
```

---
- [How to detect if system has IPv6 enabled in a UNIX shell script?](http://stackoverflow.com/questions/39983121/how-to-detect-if-system-has-ipv6-enabled-in-a-unix-shell-script/39983364#39983364)
- [Appendix D. The sysconfig Directory](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/ch-The_sysconfig_Directory.html) look for note under _D.1.13. /etc/sysconfig/network_ how to disable IPv6
