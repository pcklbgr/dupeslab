### Convert _.ppk_ to _rsa_ key

`puttygen` (installed via `putty` on RH/CentOS or `putty-tools` package on Debian) can do that:

```
puttygen id_dsa.ppk -O private-openssh -o id_dsa
```

---
- [How to convert .ppk key to OpenSSH key under Linux?](https://superuser.com/a/232365)
