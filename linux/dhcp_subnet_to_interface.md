### DHCP Subnet to Interface Mapping

On startup DHCP server will match `subnet` declarations with ip addresses assigned to interfaces.
Only interfaces that have ip's that mathc `subnet` declaration will serve requests.

DHCP server refuses to start if there are no `subnet` declarations,
lack of matching subnet and ip address pairs is treated as lack of `subnet` declarations.

---
- [dhcpd fails to start on eth1](http://askubuntu.com/questions/57155/dhcpd-fails-to-start-on-eth1/57160#57160)
