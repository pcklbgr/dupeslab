### `curl`

#### Display HTTP Status Code

To display HTTP (or FTP for that matter) status code only `-w` (`--write-out`) flag can be used.
This also shows full response body, unless it is written to a file with `-o`.

```
curl -sL -w "%{http_code}\\n" "URL" -o /dev/null
```

---
- [How To Display Just The HTTP Response Code In Command Line Curl](http://beerpla.net/2010/06/10/how-to-display-just-the-http-response-code-in-cli-curl/)
- [man page](https://curl.haxx.se/docs/manpage.html)


#### Display Headers

Dumps the headers to _stdout_ and data to _/dev/null_ (`-` tells `curl` to use _stdout_ as output file).

```
curl -s -D - www.google.com -o /dev/null
```

---
- `curl` [manpage](https://linux.die.net/man/1/curl)
- [Getting only response header from HTTP POST using curl](http://stackoverflow.com/questions/10060098/getting-only-response-header-from-http-post-using-curl/10060342#10060342)
