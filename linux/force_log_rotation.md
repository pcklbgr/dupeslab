### Force Log Rotation

```
logrotate --force $CONFIG_FILE
```

`--force` will rotate files even if `logrotate` doesn't think that is necessary
(e.g. files does not meet specified criteria for _minsize_, _age_, etc.).

---
- [Is it possible to run one logrotate check manually?](http://stackoverflow.com/a/2117789/462206)
- `logrotate` [manpage](https://linux.die.net/man/8/logrotate)
