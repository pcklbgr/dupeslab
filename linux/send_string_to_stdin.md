### Send String to `stdin`

Pipe `echo` output:

```
echo 'direct pipe to stdin.' | cat
```

Use one-line `heredoc`:

```
cat <<< "coming from the stdin"
```

---
- [Send string to stdin](http://stackoverflow.com/a/6541324)
