### Flush `nscd` DNS Cache

```
nscd --invalidate=hosts
```

---
- [How to really flush the various nscd caches](https://stijn.tintel.eu/blog/2012/05/10/how-to-really-flush-the-various-nscd-caches)
