### Concatenate Multiple Lines

Use `tr` (only hadles single character translation) to translate newline characters (e.g. to spaces):

```
cat file_with_lines | tr '\n' ' '
```

Use `awk` to change output record separator (this will work with multiple characters):

```
cat file_with_lines | awk '{print}' ORS='" '
```

---
- `tr` [manpage](https://linux.die.net/man/1/tr)
- [Output Separators](https://www.gnu.org/software/gawk/manual/html_node/Output-Separators.html)
- [How to concatenate multiple lines of output to one line?](http://stackoverflow.com/a/15580184)
