### `rpm` Packages

#### Reinstall Package

Use `--replacepkgs` flag while installing:

```
rpm -iv --replacepkgs centos-release-7-3.1611.el7.centos.x86_64.rpm
```

#### Find Package That Provides File

`rpm -qf` can be used to look through _installed_ packages:

```
rpm -qf /bin/ls
```

`yum whatprovides` searches through installed and available in the repositories packages:

```
yum whatprovides '*bin/grep'
```

To search through installed packages only:

```
yum --disablerepo=* whatprovides '*bin/grep'
```

`repoquery` tool (available in `yum-utils` package) offers simpler output and better speed:

```
repoquery --whatprovides '*bin/grep'
```

---
- [How do I find which rpm package supplies a file I'm looking for?](http://stackoverflow.com/a/2507087/462206)


#### Show `rpm` file information

```
rpm -qip PACKAGE.rpm
```

---
- [Displaying information on non-installed RPM package?](http://stackoverflow.com/a/3005902/462206)


#### List Files in `rpm` Package

List files in _.rpm_ file:

```
rpm -qpl PACKAGE-NAME.rpm
```

List files in installed package:

```
rpm -ql PACKAGE-NAME
```

---
- [man page](http://linux.die.net/man/8/rpm)


#### List _requires_ for `rpm` file

```
rpm -qp PACKAGE.rpm --requires
```

`yum deplist` will list dependencies and packages that provide needed dependencies.

---
- [Check RPM dependencies](http://stackoverflow.com/a/19087701/462206)


#### List _provides_ in `rpm` file

```
rpm -qp PACKAGE.rpm --provides
```

---
- [Check RPM dependencies](http://stackoverflow.com/a/19087701/462206)


#### Extract `rpm` File

```
rpm2cpio PACKAGE.x86_64.rpm | cpio -idmv
```

---
- [Inspecting and extracting RPM package contents](http://blog.packagecloud.io/eng/2015/10/13/inspect-extract-contents-rpm-packages/)
- [Using rpm2cpio](http://www.rpm.org/max-rpm/s1-rpm-miscellania-rpm2cpio.html)
- `cpio` [manpage](https://linux.die.net/man/1/cpio)


#### `rpm` Query

Query tags

```
rpm --querytags
```

Query format that matches `rpm -qa`:

```
rpm -qa --qf "%{NAME}-%{VERSION}-%{RELEASE}.%{ARCH}\n"
```

Query format that matches one used by `yum versionlock`:

```
rpm -qa --qf "%|EPOCH?{%{EPOCH}}:{0}|:%{NAME}-%{VERSION}-%{RELEASE}.*\n" rabbitmq-server
```

---
- [Tags: Data Definitions](http://www.rpm.org/max-rpm/s1-rpm-inside-tags.html)


#### Installed _gpg_ Signatures Information

```
for gpg in `rpm -qa gpg-pubkey`; do rpm -qi $gpg; done
```


#### List `yum` variables

```python
python -c 'import yum, pprint; yb = yum.YumBase(); pprint.pprint(yb.conf.yumvar, width=1)'
```
