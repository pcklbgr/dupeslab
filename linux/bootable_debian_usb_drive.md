### Bootable Debian USB Drive

Debian Live images are created using [_isohibrid_](http://www.syslinux.org/wiki/index.php?title=Isohybrid) an can be simply written to USB drive with e.g. `dd`:

``` bash
dd if=debian-netinst.iso of=/dev/sdX bs=4M; sync
```

`sync` ensures that all writes are flushed before command finishes.

---
- [Debian CD FAQ](https://www.debian.org/CD/faq/#write-usb)
