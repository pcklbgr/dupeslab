### Reload `.bashrc`

Use `exec bash` to replace current shell with bash (with updated configuration files).

Alternativelly - source `.bashrc`:

```
source ~/.bashrc
```

---
- [How do I reload .bashrc without logging out and back in?](http://stackoverflow.com/a/9584378)
