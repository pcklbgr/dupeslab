### `cp`

#### Copy if File Exists

Suppresses errors about missing file and exit code.

```
cp ./src/file ./dest/file 2>/dev/null || :
```

---
- [How to let 'cp' command don't fire an error when source file does not exist?](http://serverfault.com/questions/153875/how-to-let-cp-command-dont-fire-an-error-when-source-file-does-not-exist/153893#153893)
- [What is the purpose of the : (colon) GNU Bash builtin?](http://stackoverflow.com/questions/3224878/what-is-the-purpose-of-the-colon-gnu-bash-builtin/3224910#3224910)
