## Disconnect `telnet` Automagically

Disconnects `telnet` after 2s:

```bash
sleep 2 | telnet $HOST $PORT
```

---
- [Testing remote TCP port using telnet by running a one-line command](https://unix.stackexchange.com/a/136937)
