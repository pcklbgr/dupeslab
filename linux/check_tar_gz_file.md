### Check _.tar.gz_ File

List the contents of tarball (and throw away the output):

```
tar -tzf foo.tar.gz >/dev/null
```

---
- `tar` [manpage](http://linuxcommand.org/man_pages/tar1.html)
- [How to check if a Unix .tar.gz file is a valid file without uncompressing?](http://stackoverflow.com/a/2001750/462206)
