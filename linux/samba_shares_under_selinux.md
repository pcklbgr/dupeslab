### Samba Shares Under SELinux

<span style="color:red">**WARNING:**</span> this does not seem to work (last tried on CentOS 6.6).

When Samba is running under SELinux (on CentOS, Red Hat and the likes) contexts need to be properly setup.

Without contexts properly setup following issues will occur:
Attempts to connect to share via `smbclient //server/share` will result in error _tree connect failed: NT_STATUS_BAD_NETWORK_NAME_ even if shares are visible with `smbclient -L //server`.

SELinux context `samba_share_t` needs to be set for Samba share directories

```bash
chcon -t samba_share_t -R /path/to/share_root
```

To make context changes permanent (survive a file system [relabel](https://www.centos.org/docs/5/html/5.2/Deployment_Guide/sec-sel-fsrelabel.html) or `restorecon`) use `semanage` to add context change to file context configuration:

```bash
semanage fcontext -a -t samba_share_t '/path/to/share_root/share(/.*)?'
```

Changes can be applied with `restorecon`

```bash
restorecon -R /path/to/share_root
```

_NOTE:_ `semanage` is in `policycoreutils-python` package on CentOS (6.6)

---
- [SELinux/samba](https://fedoraproject.org/wiki/SELinux/samba)
- [SAMBA Setup](https://wiki.centos.org/HowTos/SetUpSamba#head-86233024cba06a1e4f554e763a2f634a61eae9b8)
- [5.6.1. Temporary Changes: chcon](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Security-Enhanced_Linux/sect-Security-Enhanced_Linux-Working_with_SELinux-SELinux_Contexts_Labeling_Files.html#sect-Security-Enhanced_Linux-SELinux_Contexts_Labeling_Files-Temporary_Changes_chcon)
- `chcon` [manpage](http://linux.die.net/man/1/chcon)
- `semanage` [manpage](http://linux.die.net/man/8/semanage)
- [Confining Samba with SELinux](http://danwalsh.livejournal.com/14195.html)
