### `find`

`find` [manpage](https://linux.die.net/man/1/find)

#### Copy Files from Subdirectories to a Folder

```
find . -type f -exec cp {} $TARGET_DIR/ \;
```

---
- [Copying multiple files from multiple directories to one folder](https://bbs.archlinux.org/viewtopic.php?pid=656208#p656208)


#### Find Files by Extension and Count Lines

```
find . -type f -iname '*.jrxml' -exec wc -l '{}' \;
```

### Find Files Older Than _n_ Days

Prints files older than 7 days:

```
find /path/to/directory/ -depth -mindepth 1 -mtime +7 -print
```

Deletes files older than 7 days:

```
find /path/to/directory/ -mindepth 1 -mtime +7 -delete
```

---
- [Delete files older than X days +](http://unix.stackexchange.com/questions/194863/delete-files-older-than-x-days/205539#205539)
