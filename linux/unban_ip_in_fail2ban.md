### Unban IP in Fail2ban

```
fail2ban-client set $JAIL unbanip $IP
```

---
- [How to Unban an IP properly with Fail2Ban](https://serverfault.com/a/336001)
