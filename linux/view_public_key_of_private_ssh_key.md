### View Public Key of Private SSH Key

```
ssh-keygen -y -f /path/to/private_key
```

---
- [`ssh-keygen`](http://linux.die.net/man/1/ssh-keygen)
- [How do I retrieve the public key from a SSH private key?](http://askubuntu.com/questions/53553/how-do-i-retrieve-the-public-key-from-a-ssh-private-key/53555#53555)
