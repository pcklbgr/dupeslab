### Check if Apache is Using Prefork or Worker

Look for mentions of `prefork.c` or `worker.c` in the output of `apachectl` (or `apache2ctl`).

```
$ apachectl -l
Compiled in modules:
  core.c
  prefork.c
  http_core.c
  mod_so.c
```

---
- [Apache Prefork vs Worker MPM](https://stackoverflow.com/a/24051935)
- [Apache MPM prefork](https://httpd.apache.org/docs/2.4/mod/prefork.html)
- [Apache MPM worker](https://httpd.apache.org/docs/2.4/mod/worker.html)
