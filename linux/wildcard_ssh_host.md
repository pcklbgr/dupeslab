### Wildcard SSH _Host_

_Hosts_ can match multiple characters with `*` or a single character with `?`.
e.g. following will match names like _app.boop.foo1_ or _db.boop.bar2_.

```
Host *.*.foo? *.*.bar?
    User user_name
```

Effectivelly wild card _Host_ can be used to add full domain name to a host:

```
Host *.*.foo?
    HostName %h.long.network.name.org
    User user_name
```

---
- `ssh` config [manpage](https://linux.die.net/man/5/ssh_config)
- [Can i configure ubuntu to append a domain to the end of an ssh hostname request?](https://askubuntu.com/a/760686)
- [Multiple similar entries in ssh config](https://unix.stackexchange.com/a/61666)
