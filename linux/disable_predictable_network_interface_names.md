### Disable _Predictable Network Interface Names_

- [Predictable Network Interface Names](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/)

### CentOS 7 Install

Add `net.ifnames=0 biosdevname=0` to the _Install CentOS 7_ in the installation boot menu.

---
- [CentOS 7 disable predictable network interface names during install](https://serverfault.com/a/692899)
- [And what if I want the old naming back?](https://wiki.centos.org/FAQ/CentOS7#head-31ebc6642958a0df12304d6aab9a49034a3b7802)
