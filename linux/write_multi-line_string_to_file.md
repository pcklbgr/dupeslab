### Write Multi-line String to File

```
cat > FILE <<- EOF
Line 1.
Line 2.
EOF
```

---
- [Multi-line string with extra space (preserved indentation)](http://stackoverflow.com/a/23930212/462206)
