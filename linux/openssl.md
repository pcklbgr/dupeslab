### OpenSSL

#### View Certificate Details

```
openssl x509 -in certificate.crt -noout -text
```

Use `-inform pem` or `inform der` to view _PEM_ or _DER_ format certificate.

---
- `openssl` (_master_) [manpage](https://www.openssl.org/docs/manmaster/man1/openssl.html)
- [How do I view the details of a digital certificate .cer file?](http://serverfault.com/questions/215606/how-do-i-view-the-details-of-a-digital-certificate-cer-file/215617#215617)
- [How do I view the details of a digital certificate .cer file?](https://serverfault.com/a/215617)
- [The Most Common OpenSSL Commands](https://www.sslshopper.com/article-most-common-openssl-commands.html)


#### View PKCS#12 Certificate

```
openssl pkcs12 -info -in domain.pfx
```

If PKCS#12 file has no _export key_ and private key is not ecrypted - interactive input can be skipped:

```
openssl pkcs12 -info -nodes -passin pass: -in direct.pfx
```


#### Check if Certificate Matches Private Key

Checkums will match if certificate matches private key:

```sh
openssl pkey -in private.key -pubout -outform pem | sha256sum
openssl x509 -in certificate.crt -pubkey -noout -outform pem | sha256sum
```

Modulus checksum is identical for matching private key, request and cert:

```sh
openssl rsa -in KEY_FILE.key -noout -modulus | openssl md5
openssl req -in CSR_FILE.csr -noout -modulus  | openssl md5
openssl x509 -in LEAF_FILE.crt -noout -modulus | openssl md5
```

---
- [Certificate Key Matcher](https://www.sslshopper.com/certificate-key-matcher.html)
- [How to check if the certificate matches a Private Key?](https://www.ssl247.com/kb/ssl-certificates/troubleshooting/certificate-matches-private-key)


#### View Certificate Signing Request Details

```
openssl req -in request.csr -noout -text
```

---
- [What is a CSR (Certificate Signing Request)?](https://www.sslshopper.com/what-is-a-csr-certificate-signing-request.html)


#### View Certificate Chain Details

```sh
openssl crl2pkcs7 -nocrl -certfile CHAIN.pem | openssl pkcs7 -print_certs -text -noout
```

Skip `-text` flag to only show subject/issuer.

---
- [How to view all ssl certificates in a bundle?](https://serverfault.com/a/755815)


#### Certificate File Formats

> .csr This is a Certificate Signing Request. Some applications can generate these for submission to certificate-authorities. The actual format is PKCS10 which is defined in RFC 2986. It includes some/all of the key details of the requested certificate such as subject, organization, state, whatnot, as well as the public key of the certificate to get signed. These get signed by the CA and a certificate is returned. The returned certificate is the public certificate (which includes the public key but not the private key), which itself can be in a couple of formats.
>
> .pem Defined in RFC's 1421 through 1424, this is a container format that may include just the public certificate (such as with Apache installs, and CA certificate files /etc/ssl/certs), or may include an entire certificate chain including public key, private key, and root certificates. Confusingly, it may also encode a CSR (e.g. as used here) as the PKCS10 format can be translated into PEM. The name is from Privacy Enhanced Mail (PEM), a failed method for secure email but the container format it used lives on, and is a base64 translation of the x509 ASN.1 keys.
>
> .key This is a PEM formatted file containing just the private-key of a specific certificate and is merely a conventional name and not a standardized one. In Apache installs, this frequently resides in /etc/ssl/private. The rights on these files are very important, and some programs will refuse to load these certificates if they are set wrong.
>
> .pkcs12 .pfx .p12 Originally defined by RSA in the Public-Key Cryptography Standards, the "12" variant was enhanced by Microsoft. This is a passworded container format that contains both public and private certificate pairs. Unlike .pem files, this container is fully encrypted. Openssl can turn this into a .pem file with both public and private keys: openssl pkcs12 -in file-to-convert.p12 -out converted-file.pem -nodes

---
- [What is a Pem file and how does it differ from other OpenSSL Generated Key File Formats?](https://serverfault.com/a/9717)


#### Convert PKCS#7 to PEM

Convert PKCS#7/P7B to PEM:

```
openssl pkcs7 -print_certs -in old.p7b -out new.crt
```

---
- [jmervine/cert_convert.sh](https://gist.github.com/jmervine/e4c9af3adb14b78856cc)
- [_pkcs7_](https://www.openssl.org/docs/manmaster/man1/pkcs7.html)
- [How to convert a certificate into the appropriate format](https://knowledge.digicert.com/solution/SO26449.html)

#### Export PEM to PKCS#12

To export PEM certificate (and it's private key) as PKCS#12:

```
openssl pkcs12 -export -out domain.pfx -inkey domain.key -in domain.crt
```

Any additional certificates that should be added can be specified as `-certfile more.crt`

`-passout pass:` can trick openssl into exporting PKCS#12 wihtout the _export key_ (same as hitting enter without any input interactivelly):

```
openssl pkcs12 -export -out domain.pfx -inkey domain.key -in domain.crt -passout pass:
```

---
- [Create a .pfx/.p12 Certificate File Using OpenSSL](https://www.ssl.com/how-to/create-a-pfx-p12-certificate-file-using-openssl/)
- [Export a PKCS#12 file without an export password?](https://stackoverflow.com/a/27497899)
