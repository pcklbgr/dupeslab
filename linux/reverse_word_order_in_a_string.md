### Reverse Word Order in a String

Using AWK `while`:

```sh
echo "foo bar baz quux" | awk '{do printf "%s"(NF>1?FS:RS),$NF;while(--NF)}'
```

To use a different word separator - pass it with `-F`:

```sh
echo "foo.bar.baz.quux" | awk -F. '{do printf "%s"(NF>1?FS:RS),$NF;while(--NF)}'
```

---
- [How to reverse a list of words in a shell string?](https://stackoverflow.com/a/27704381)
- [How to use “:” as awk field separator?](https://stackoverflow.com/a/2609565)
