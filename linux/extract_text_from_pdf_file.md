### Extract Text from _pdf_ File

Text can be extracted from _pdf_ file with ghostscript (works well on _cygwin_).

```
gs -sDEVICE=txtwrite -o output.txt input.pdf
```

---
- [How to extract text from a PDF?](http://stackoverflow.com/a/26405241/462206)
- [How to use Ghostscript](http://www.ghostscript.com/doc/9.19/Use.htm)
