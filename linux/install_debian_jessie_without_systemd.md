### Install Debian Jessie Without _systemd_

Add to the boot arguments (by hitting TAB at the boot menu):

```
preseed/late_command="in-target apt-get install -y sysvinit-core"
```

Some _systemd_ packages will be installed, especially if desktop manager is installed as _lightdm_ has a dependency to _libsystemd0_, but _SysVinit_ will handle init.

---
- _systemd_ Debian [wiki](https://wiki.debian.org/systemd#Installing_without_systemd)
