### Convert _.flac_ to _.mp3_

Install `libav-tools` (this should also install _lame_ and _flac_ codecs).

Run in a folder with _.flac_ files that need to be converted.

```
for f in *.flac; do avconv -i "$f" -c:a libmp3lame -b:a 320k "${f[@]/%flac/mp3}" ; done
```

For 320 kbps CBR use `-c:a libmp3lame -b:a 320k`

For VBR use `-qscale:a 0`

---
- [What is the proper way to convert .flac files to 320 kBit/sec .mp3?](http://askubuntu.com/a/385656)
- _Convert FLAC to MP3_ Arch Linux [wiki](https://wiki.archlinux.org/index.php/Convert_FLAC_to_MP3)
