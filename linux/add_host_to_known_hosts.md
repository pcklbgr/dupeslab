### Add Host to `known_hosts`

```
ssh-keyscan REMOTE_HOST >> ~/.ssh/known_hosts
ssh-keyscan -p PORT REMOTE_IP >> ~/.ssh/known_hosts
```

_Note:_ this can be used before `git clone` to stop `git` from asking to add new host.

---
- [Can I automatically add a new host to known_hosts?](http://serverfault.com/a/316100)
- `ssh-keyscan` [manpage](https://www.freebsd.org/cgi/man.cgi?query=ssh-keyscan&sektion=1)
