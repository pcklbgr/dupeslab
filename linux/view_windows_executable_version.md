### View Windows Executable Version

[`pev`](http://pev.sourceforge.net/) can be used to view version of windows executable:

```
pev -p executable.exe
```

---
- [How to view a PE EXE/DLL file version information?](http://askubuntu.com/questions/23454/how-to-view-a-pe-exe-dll-file-version-information/48993#48993)
