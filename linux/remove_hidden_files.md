### Remove Hidden Files

Removes all hidden files and directories:

```
rm -rf .[^.] .??*
```
