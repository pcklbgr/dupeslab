### Trigger Creation of `70-persistent-net.rules`

```
udevadm trigger --subsystem-match=net
```

---
- [Manually trigger creation of /etc/udev/rules.d/70-persistent-net.rules file](https://access.redhat.com/discussions/1240213)
