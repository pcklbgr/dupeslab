### Recursive `wget`

To download files recursively use flag `-r` (`--recursive`):

```
wget -r http://cdimage.debian.org/debian-cd/8.4.0/amd64/bt-cd/
```

Usualy it is useful to prevent `wget` from ascending up to the parent directory and downloading more that you need:

```
wget -r -nH -np -nd -R "*-update-*" -A "torrent" http://cdimage.debian.org/debian-cd/8.4.0/amd64/bt-cd/
```

- `-nh` (`--no-host-directories`): disables generation of host-prefixed directories.
- `-np` (`--no-parent`): `wget` does not ascend to the parent directory.
- `-nd` (`--no-directories`): `wget` does not create hierarchy of directories.
- `-A`/`-R` (`--accept`/`--reject`): comma separated lists of file name suffixes or parterns to accept/reject.

---
- [Using wget to recursively fetch a directory with arbitrary files in it](http://stackoverflow.com/questions/273743/using-wget-to-recursively-fetch-a-directory-with-arbitrary-files-in-it/273776#273776)
- [man](http://linux.die.net/man/1/wget) page
