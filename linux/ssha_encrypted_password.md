### _SSHA_ Encrypted Password

_SSHA_ encrypted password can be used in `.htpasswd` files.

```bash
PASS='ShhSecret';

SALT="$(openssl rand -base64 3)"
SHA1=$(printf "$PASS$SALT" | openssl dgst -binary -sha1 | sed 's#$#'"$SALT"'#' | base64);

printf "{SSHA}$SHA1\n"
```

[surjikal/encrypt-pw-ssha.sh](https://gist.github.com/surjikal/5331916) can be used as ready to run solution.
