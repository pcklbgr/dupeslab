### Parse Filename from URL

```
URL="http://www.foo.bar/file.ext"
FILE="${URL##*/}"
```

---
- [Extract the base file name from a URL using bash](http://unix.stackexchange.com/a/64435)
