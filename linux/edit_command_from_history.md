### Edit Command from `history`

To print command from `history` (command is entered in history and can be edited by pressing _up_):

```
!42:p
```

---
- [put history command onto command line without executing it](https://unix.stackexchange.com/a/4086)
