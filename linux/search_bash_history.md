### Search Bash History

Hit `ctrl+r` and start typing to use `reverse-search-history`.

---
- [Commands For Manipulating The History](https://www.gnu.org/software/bash/manual/bashref.html#Commands-For-History) in `bash` manpage
- [How can I search the bash history and rerun a command?](https://superuser.com/a/7416)
