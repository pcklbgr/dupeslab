### Switch PulseAudio Output

List available outputs:

```sh
pactl list short sinks
```

List sources:

```sh
pactl list short sink-inputs
```

Switch output:

```sh
move-sink-input $SOURCE $OUTPUT
```

---
- `pactl` [manpage](https://manpages.ubuntu.com/manpages/xenial/man1/pactl.1.html)
- [Change PulseAudio Input/Output from Shell?](https://unix.stackexchange.com/a/67398)
