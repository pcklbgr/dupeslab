### `git` Information in Bash Shell Prompt

Download [`git-prompt.sh`](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh)

```
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -O .git_prompt.sh
```

Update shell prompt (`$PS1`) to use it in [`.bash_profile`](difference_between_.basrc_and_.bash_profile.md):

```
. ~/.git_prompt.sh
export PS1='\[\e]0;\W\a\]\n\[\e[32m\]\u@\h \[\e[33m\]\W\[\e[0m\]$(__git_ps1 " (%s)")\$ '
```

---
- `bash` [manpage](http://man7.org/linux/man-pages/man1/bash.1.html#PROMPTING)
- [Git in Other Environments - Git in Bash](https://git-scm.com/book/uz/v2/Git-in-Other-Environments-Git-in-Bash)
- [How do I modify cygwin's PS1 for git bash completion?](http://stackoverflow.com/a/21141058)
