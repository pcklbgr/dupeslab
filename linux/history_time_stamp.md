### `history` Time Stamp

Time stamp can be changed with `HISTTIMEFORMAT` variable:

```
HISTTIMEFORMAT="%Y%m%y %T "
```

Add it to `.bash_profile` to make it stick

```
echo 'export HISTTIMEFORMAT="%Y%m%y %T "' >> ~/.bash_profile
```

---
- [How to see time stamps in bash history?](http://askubuntu.com/questions/391082/how-to-see-time-stamps-in-bash-history)
- `date` [manpage](https://linux.die.net/man/1/date)
