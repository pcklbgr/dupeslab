### File lines

Different lines between files:

```
grep -Fxvf FOO.file BAR.file
```

Unique (and sorted) lines in a file:

```
sort -u FOO.file
```

Nth line of a file:

```
cat LICENSE | sed -n '3p'    # 3rd line

cat LICENSE | sed -n '3,5p'  # 3-5th lines
```
