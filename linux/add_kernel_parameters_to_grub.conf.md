### Add Kernel Parameters to `grub.conf`

```
grubby --update-kernel=ALL --args="transparent_hugepage=never
```

Parameters are not duplicated and should survive reboot, if kernel package install scripts take care to keep current kernel parameters.

_NOTE:_ `grubby` looks to be primarily available on RHEL & derivatives.

---
- `grubby` [manpage](https://linux.die.net/man/8/grubby)
- [How do I automatically add kernel parameters into grub.conf?](https://access.redhat.com/articles/1301)
