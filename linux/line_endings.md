### File Lines

#### List Files with Specific Line Endings

```
egrep -l $'\r'\$ *      # files with DOS style line endings
egrep -L $'\r'\$ *      # files with UNIX style line endings
```

---
- [How to determine the line ending of a file](http://stackoverflow.com/a/121464/462206)

#### Convert Line Endings

Changes line endings inline.

```
perl -pi -e 's/\r$//' foo.dos        # DOS => UNIX style line endings
perl -pi -e '/s\n/\r\n/' foo.nix     # UNIX => DOS style line endings
```

Removes [UTF-8 with BOM](https://en.wikipedia.org/wiki/Byte_order_mark#UTF-8):

```
sed -i '1 s/^\xef\xbb\xbf//' $CF
```
