### Disable Printing on Samba

To disable pringing (connecting to CUPS) add following to the _global_ section of `smb.conf`:

```ini
  load printers = no
  printcap name = /dev/null
  disable spoolss = yes
```

By default Samba attempts to collect printers from CUPS and fails to do so with errors:

> ```
> [2016/05/24 17:02:32.584649,  0] printing/print_cups.c:151(cups_connect)
>  Unable to connect to CUPS server localhost:631 - Connection refused
> [2016/05/24 17:02:32.585019,  0] printing/print_cups.c:528(cups_async_callback)
>  failed to retrieve printer list: NT_STATUS_UNSUCCESSFUL
> ```

---
- [SAMBA Server disable CUPS](http://antmeetspenguin.blogspot.lt/2013/08/samba-server-disable-cups.html)
