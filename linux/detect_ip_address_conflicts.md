### Detect IP Address Conflicts

Install `arp-scan` package (in EPEL on CentOS).

To detect IP address conflicts run (as root):

```bash
arp-scan -I eth0 -l
```

---
- [How to detect IP address conflicts in Linux](http://xmodulo.com/how-to-detect-ip-address-conflicts-in.html)
- `arp-scan` [manpage](http://linux.die.net/man/1/arp-scan)
- [Address Resolution Protocol](https://en.wikipedia.org/wiki/Address_Resolution_Protocol)
