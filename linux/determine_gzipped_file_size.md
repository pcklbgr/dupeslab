### Determine gzip'ed File Size

Using `zcat`:

```
zcat file.gz | wc -c
```

Using `gzip`:

```
gzip -dc file.gz | wc -c
```

---
- [How can I get the uncompressed size of gzip file without actually decompressing it?](https://superuser.com/a/828300)
