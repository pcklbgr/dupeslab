## Ping with _MTU 9000_ packets

```sh
ping -M do -s 8972 10.0.0.1
```

---
- [How to test if 9000 MTU/Jumbo Frames are working](https://blah.cloud/hardware/test-jumbo-frames-working/)
- [how to verify if MTU 9000 configured properly on all component](https://unix.stackexchange.com/a/466981))
