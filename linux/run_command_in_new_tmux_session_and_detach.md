### Run Command in New `tmux` Session and Detach

```
tmux new -d -s SESSION 'COMMAND'
```

---
- `tmux` [manpage](http://man.openbsd.org/OpenBSD-current/man1/tmux.1)
