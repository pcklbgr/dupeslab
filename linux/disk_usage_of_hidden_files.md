### Disk Usage of Hidden Files

```
du -sch .[!.]* *
```

It will throw errors if there are no hidden files, disk usage of non-hidden files is still shown.

---
- [Why doesn't this show the hidden files/folders?](https://askubuntu.com/a/363681)
