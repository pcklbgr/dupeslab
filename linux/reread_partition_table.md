### Reread Partition Table

To reread partition table of particular drive use

``` bash
hdparm -z /dev/sdx
```
