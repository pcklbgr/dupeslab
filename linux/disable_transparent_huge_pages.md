### Disable Transparent Huge Pages

To check if Transparent Huge Pages (THP) are enabled:

```
cat /sys/kernel/mm/transparent_hugepage/enabled
```

To disable THP during runtime:

```bash
[ -f "/sys/kernel/mm/transparent_hugepage/enabled" ] && echo never > /sys/kernel/mm/transparent_hugepage/enabled
```

[Add kernel parameter](add_kernel_parameters_to_grub.conf.md) `transparent_hugepage=never` to disable THP at boot time.

---
- [Huge Pages And Transparent Huge Pages](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/performance_tuning_guide/s-memory-transhuge)
- [How to use, monitor, and disable transparent hugepages in Red Hat Enterprise Linux 6 and 7?](https://access.redhat.com/solutions/46111)
