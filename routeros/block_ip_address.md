### Block IP Address

Add blocked IP addresses to address list:

```
/ip firewall address-list add list=drop_traffic address=192.0.34.166/32
/ip firewall address-list add list=drop_traffic address=10.11.12.13
```

Create firewall rule to drop trafic coming from ip addresses in the list

```
/ip firewall filter add chain=input src-address-list=drop_traffic action=drop
```

---
- [Manual:IP/Firewall/Filter](http://wiki.mikrotik.com/wiki/Manual:IP/Firewall/Filter)
- [Manual:IP/Firewall/Address list](http://wiki.mikrotik.com/wiki/Manual:IP/Firewall/Address_list)
- [How to block ip address in MT](http://forum.mikrotik.com/viewtopic.php?t=31994#p485045)
