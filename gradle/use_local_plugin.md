### Use Local Plugin

Put plugin files to a local Maven repo (repo is created in a subdirectory).

```groovy
uploadArchives {
    repositories {
        mavenDeployer {
            repository(url: uri('../repo'))
        }
    }
}
```

Point gradle build to local Maven repo.

```groovy
buildscript {
    repositories {
        maven {
            url uri('../repo')
        }
    }
    dependencies {
        classpath group: 'org.gradle', name: 'customPlugin', version: '1.0-SNAPSHOT'
    }
}
```

---
- _Plugin_ [`build.gradle`](https://github.com/gradle/gradle/blob/master/subprojects/docs/src/samples/customPlugin/plugin/build.gradle#L31-L37)
- _Consumer_ [`build.gradle`](https://github.com/gradle/gradle/blob/master/subprojects/docs/src/samples/customPlugin/consumer/build.gradle#L4-L8)
