### Delete File Tree

`fileTree` can be passed directly to `delete`.

```groovy
delete fileTree('doc') {
    include '**/*.pdf'
    include '**/*.html'
    exclude '**/images/*.pdf'
}
```

---
- [File trees](https://docs.gradle.org/current/userguide/working_with_files.html#sec:file_trees) on [Gradle User Guide](https://docs.gradle.org/current/userguide/userguide.html)
- [Delete](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.Delete.html#N1C3DF) method on [Gradle Build Language Reference](https://docs.gradle.org/current/dsl/)
