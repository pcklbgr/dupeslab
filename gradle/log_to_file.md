### Log to File

```groovy
def log = file("${rootProject.projectDir.getPath()}/build.log")

gradle.services.get(LoggingOutput).addStandardOutputListener (
  new StandardOutputListener () {
    void onOutput(CharSequence output) {
      log << output
    }
})

gradle.services.get(LoggingOutput).addStandardErrorListener (
  new StandardOutputListener () {
    void onOutput(CharSequence output) {
      log << output
    }
})
```

**NOTE:** Using `LoggingOutputInternal` (as described [here](http://willis7.github.io/blog/2013/gradle-output-to-log.html)) is no loger possible due to [changes](https://docs.gradle.org/2.14/release-notes#better-isolation-of-internal-gradle-classes-with-gradleapi()) in version _2.14_

---
- Interface [`LoggingOutput`](https://docs.gradle.org/current/javadoc/org/gradle/api/logging/LoggingOutput.html)
