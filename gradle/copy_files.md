### Copy Files

Copies files collected by _File Tree_; foregoes up-to-date check.

```groovy
task CopyFiles << {
    copy {
        from files{ fileTree(dir: 'rpm').matching { include 'RPMS/**/*.rpm' } }.files
        into 'artifacts'
    }
}
```

---
- [Copying files](https://docs.gradle.org/current/userguide/working_with_files.html#sec:copying_files)
