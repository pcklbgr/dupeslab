### Paste with `shift-insert` from Ditto

Add to `Ditto.settings`:

```ini
[PasteStrings]
putty.exe=+{INS}
ConEmu64.exe=+{INS}
```

---
- [Putty support](https://sourceforge.net/p/ditto-cp/discussion/287511/thread/c6789e56/#4a52)
