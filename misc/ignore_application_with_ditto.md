### Ignore Application with Ditto

To ignore clipboard changes from specific application update _Accepted Copy Applications_ _Exclude_ field (under _General_ tab in settings):

![accepted copy applications - exclude](ditto_exclude.png)

Or update `Ditto.settings`

```ini
[Ditto]
CopyAppExclude=KeePass.exe
```
