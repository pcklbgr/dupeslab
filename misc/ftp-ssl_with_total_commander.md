### FTP-SSL with Total Commander

Total Commander needs OpenSSL binaries to work with [FTP-SSL](https://en.wikipedia.org/wiki/FTPS).

![Total Commander FTP connection details](total_commander_ftp-ssl.png)

OpenSSL binaries need to be copied to Total Commander installation directory.

For 32bit Total Commander:

```
libssl32.dll
libeay32.dll
```

For 64bit Total Commander:

```
ssleay32.dll
libeay32.dll
```

Recent(ish) versions of OpenSSL binaries for Windows can be obtained [here](https://indy.fulgan.com/SSL/).

---
- [OpenSSL DLLs for Total Commander](http://www.miroslavnovak.com/ftps-total-commander-openssl-dll_en.php)
