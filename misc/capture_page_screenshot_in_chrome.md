### Capture Page Screenshot in Chrome

Launch _Developer Tools_ (`F12`, `Ctrl`+`Shift`+`i` or _More tools_ > _Developer tools_ in the menu).

Open _Command menu_ (`Ctrl`+`Shift`+`p`) and type in _screenshot_ and select *Capture full size screenshot*.

![Capture screenshot](chrome_screenshot.png)

*NOTE:* this requires at least _Chrome 59_

---
- [How to take full page screenshot in Chrome without extensions](https://www.utilitylog.com/full-page-screenshot-chrome/)
- [Capture full webpage screenshots in Chrome 59 without extensions](http://www.ithinkdiff.com/capture-full-webpage-screenshots-in-chrome-59-without-extensions/)
