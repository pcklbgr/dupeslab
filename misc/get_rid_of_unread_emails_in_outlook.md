### Get Rid of Unread Emails in Outlook

- Search for `read:no` (in the _Search Current Mailbox (Ctrl+E)_ box).
- Click _Find More on Server_ link; unread email should show up.

---
- [How to fix the wrong number of unread emails flag in Outlook?](https://superuser.com/a/861527)
