### `npm` Loglevel

`npm` has couple of log levels [available](https://docs.npmjs.com/misc/config#loglevel):

> "silent", "error", "warn", "http", "info", "verbose", "silly"

e.g. `npm --loglevel silent install`

There's also couple of cli [shorthands](https://docs.npmjs.com/misc/config#shorthands-and-other-cli-niceties) related to loglevel.
