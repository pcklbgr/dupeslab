### REST API on _AVE 7.3-233_

Install REST API .rpm (contact [EMC](support.emc.com) to get the .rpm file)

```
rpm -ivh rest-api-7.3.0-233.sles11_64.x86_64.rpm
```

Set `provider.password` in `/usr/local/avamar/var/rest-api/server_data/restserver.properties` and start REST API server

```
/usr/local/avamar/bin/restserver.sh --start
```

`provider.password` should be encrypted now:

```
$ grep "provider.password=" /usr/local/avamar/var/rest-api/server_data/restserver.properties
provider.password=ENC(oX815Hi8agQ7rrv6F40YYINkzL6qVHOP)
```

Attempt a login to REST API to check if it works:

```
$ curl -k -D- --user admin:SECRET_PASSWORD -X POST https://$AVE-HOST:8543/rest-api/login
HTTP/1.1 201 Created
X-Concerto-Authorization: OGQ4NjNiODItZjA0MC00MTE0LTk4OGEtZDFkOGRjZTdiZWVi
Date: Fri, 25 Nov 2016 10:03:34 GMT
Content-Type: application/xml; version=1.0
Transfer-Encoding: chunked
Connection: close
Server: Avamar

<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Session xmlns="http://www.emc.com/concerto/v1.0" href="https://$AVE-HOST:8543/rest-api/session" type="application/xml,application/json"><User name="admin"/><AccessPoint href="https://$AVE-HOST:8543/rest-api/admin/provider/67765db4-dda4-434f-b019-6161da604e1e" id="67765db4-dda4-434f-b019-6161da604e1e" name="Root"/></Session>
```

Further API documentation can be found on `http://$AVE-HOST:8580/rest-api-doc`

#### Fix `curl: (35) SSL connect error`

Needs _root_ permissions.
Download latest _jsafe_ libraries:

```
wget ftp://avamar_ftp:anonymous@ftp.avamar.com/software/jsafe-6.2_v0002.tgz
tar --zxvf jsafe-6.2_v0002.tgz
```

Stop tomcat and related services:

```
emwebapp.sh --stop
```

Update _jsafe_ libraries:

```
mv *jsafe* /usr/local/avamar-tomcat/lib/
cd /usr/local/avamar-tomcat/lib/
./replace_jsafe_libraries.pl
```

Start tomcat and related services (it might take ~5 min. for services to come up):

```
emwebapp.sh --start
```

`curl -k -D- --user admin:SECRET_PASSWORD -X POST https://$AVE-HOST:8543/rest-api/login` should now return _HTTP 201_.

---
- [Rest API issue Avamar VE 7.3.0-233](https://community.emc.com/message/953095)
- [libcurl error codes](https://curl.haxx.se/libcurl/c/libcurl-errors.html)
