### Set `$PATH`

*NOTE:* this applies to _declarative pipeline_ scripts.

Using `enviromnent` directive

```
pipeline {
    agent { label 'slave' }
    environment {
        PATH = "/opt/php/bin:$PATH"
    }
    stages {
        stage ('build') {
            steps {
                echo "PATH is: $PATH"
            }
        }
    }
}
```

---
- [`environment` directive](https://jenkins.io/doc/book/pipeline/syntax/#environment)
- [How to set PATH in Jenkins Declarative Pipeline](https://stackoverflow.com/a/45101214)
