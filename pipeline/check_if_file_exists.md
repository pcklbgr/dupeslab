### Check if File Exists

```groovy
fileExists('file.foo')
```

---
- [`fileExists`](https://jenkins.io/doc/pipeline/steps/workflow-basic-steps/#code-fileexists-code-verify-if-file-exists-in-workspace)
