### Print File to Output

```groovy
echo readFile('file.foo')
```

---
- [`readFile`](https://jenkins.io/doc/pipeline/steps/workflow-basic-steps/#code-readfile-code-read-file-from-workspace)
