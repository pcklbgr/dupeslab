### Get Command Output

Use `returnStdout: true` in `sh` step:

```groovy
output = sh script: "echo 'FOO BAR BAZ QUUX'", returnStdout: true
```

In _declarative pipeline_ `script { .. }` block is required:

```
pipeline {
    agent { label 'slave' }
    stages {
        stage ('build') {
            steps {
                script {
                    // will trim newline
                    output = sh(script: "echo 'FOO BAR BAZ QUUX'", returnStdout: true).trim()
                }
                echo output
            }
        }
    }
}
```

---
- `sh` [step](https://jenkins.io/doc/pipeline/steps/workflow-durable-task-step/#code-sh-code-shell-script)
- `script` [block](https://jenkins.io/doc/book/pipeline/syntax/#script)
- [Jenkins: Cannot define variable in pipeline stage](https://stackoverflow.com/a/39846733)
- [Is it possible to capture the stdout from the sh DSL command in the pipeline](https://stackoverflow.com/a/38912813)
