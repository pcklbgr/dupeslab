### Remount Partition with New Options

Use `-u` to update mounted partition and `-o` to pass new options:

```
mount -u -o acls /var/log
```

---
- FreeBSD's `mount` [manpage](https://www.freebsd.org/cgi/man.cgi?mount%288%29)
