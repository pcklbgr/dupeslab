### CPU Info

```
sysctl hw.model hw.machine hw.ncpu
```

---
- [FreeBSD CPU Information Command](https://www.cyberciti.biz/faq/howto-find-out-freebsd-cpuinfo/)
