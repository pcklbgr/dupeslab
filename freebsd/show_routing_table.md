### Show Routing Table

```
netstat -r
```

---
- [Gateways and Routes](https://www.freebsd.org/doc/handbook/network-routing.html) in the _FreeBSD Handbook_
