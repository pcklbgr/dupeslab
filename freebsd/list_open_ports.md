### List Open Ports

List all open _IPv4_ ports.

```
sockstat -4 -l
```

---
- `sockstat` [manpage](https://www.freebsd.org/cgi/man.cgi?query=sockstat&sektion=1)
- [FreeBSD List / Display Open Ports With sockstat Command](https://www.cyberciti.biz/tips/freebsd-lists-open-internet-unix-domain-sockets.html)
