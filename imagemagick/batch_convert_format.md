### Batch Convert Format

Use ImageMagick [`mogrify`](http://www.imagemagick.org/script/mogrify.php) tool (e.g. covert all _.bmp_ files to _.png_):

```
mogrify -monitor -format png *.bmp
```
