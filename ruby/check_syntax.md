### Check Syntax

Check syntax of a ruby script without executing it:

```
ruby -c foo.rb
```

---
- [Check the Syntax of a Ruby Script](http://rubyquicktips.com/post/9824742941/check-the-syntax-of-a-ruby-script)
