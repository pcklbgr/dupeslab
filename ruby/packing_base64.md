### Packing/Unpacking _base64_

Packing:

``` ruby
data = 8.times.map{ rand(256) }       # => [3, 84, 106, 18, 34, 105, 97, 218]
bins = data.pack('C*')                # => "\x03Tj\x12\"ia\xDA"
encs = Base64.encode64(bins)          # => "A1RqEiJpYdo=\n"
```

Unpacking

``` ruby
encs = "A1RqEiJpYdo=\n"               # => "A1RqEiJpYdo=\n"
bins = Base64.decode64(encs)          # => "\x03Tj\x12\"ia\xDA"
data = bins.unpack('C*')              # => [3, 84, 106, 18, 34, 105, 97, 218]
```

---
- [stackoverflow](http://stackoverflow.com/questions/7618598/unpack-base64-encoded-32bit-integer-representing-ip-address/12223833#12223833)
- ruby 1.8.7 [Base64](http://ruby-doc.org/stdlib-1.8.7/libdoc/base64/rdoc/Base64.html)
- ruby 1.8.7 [Array.pack](http://ruby-doc.org/core-1.8.7/Array.html#method-i-pack)
- ruby 1.8.7 [String.unpack](http://ruby-doc.org/core-1.8.7/String.html#method-i-unpack)
