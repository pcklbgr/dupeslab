### Shards

#### Show Shard Allocation

Number of shards allocated to nodes (and node disk usage):

```
curl -k -XGET 'https://localhost:9200/_cat/allocation?v'
```

This will show the number of unallocated shards if there are any.

---
- [cat allocation API](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-allocation.html)

#### List _UNASSIGNED_ Shards

```
curl -s -k -XGET 'https://localhost:9200/_cat/shards?h=index,shard,prirep,state,unassigned.reason' | grep -i unassigned
```

The name of the index, shard number, whether it is a primary (_p_) or replica (_r_) shard, and the reason it is unassigned are listed.

---
- [Pinpointing problematic shards](https://www.datadoghq.com/blog/elasticsearch-unassigned-shards/#pinpointing-problematic-shards)

#### Retry Shard Allocation

To retry shard allocation (e.g. to re-allocate shards that failed due to not enough disk space):

```
curl -k -XPOST 'https://localhost:9200/_cluster/reroute?retry_failed=true&pretty'
```

---
- [Cluster Reroute](https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-reroute.html)
