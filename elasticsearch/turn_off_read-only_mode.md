### Turn Off Read-Only Mode

To turn off read-only mode:

```
curl -k -XPUT -H 'Content-Type: application/json' 'https://localhost:9200/_all/_settings' -d '{"index.blocks.read_only_allow_delete": null}'
```

Elasticsearch puts itself into read-only mode when disk is running out of space to prevent data corruption.

---
- [Elasticsearch error: cluster_block_exception [FORBIDDEN/12/index read-only / allow delete (api)], flood stage disk watermark exceeded](https://stackoverflow.com/a/50609418)
- [Disk-based shard allocation](https://www.elastic.co/guide/en/elasticsearch/reference/current/disk-allocator.html)
